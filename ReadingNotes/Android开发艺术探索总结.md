#### Activity(Fragment)的生命周期和启动模式

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F%E5%88%86%E6%9E%90 "生命周期分析")生命周期分析

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%85%B8%E5%9E%8B%E7%9A%84%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F "典型的生命周期")典型的生命周期

    1. **activity的生命周期**

        [![](http://7xrk8u.com1.z0.glb.clouddn.com/activity_lifecycle.png)](http://7xrk8u.com1.z0.glb.clouddn.com/activity_lifecycle.png)

        [![](http://7xrk8u.com1.z0.glb.clouddn.com/activity_lifecycle_table.png)](http://7xrk8u.com1.z0.glb.clouddn.com/activity_lifecycle_table.png)

    2. **fragment的生命周期**

        [![](http://7xrk8u.com1.z0.glb.clouddn.com/fragment_lifecycle.png)](http://7xrk8u.com1.z0.glb.clouddn.com/fragment_lifecycle.png)

    3. **与宿主Activity的生命周期的关系**

        [![](http://7xrk8u.com1.z0.glb.clouddn.com/activity_fragment_lifecycle.png)](http://7xrk8u.com1.z0.glb.clouddn.com/activity_fragment_lifecycle.png)

        [![](http://7xrk8u.com1.z0.glb.clouddn.com/activity_fragment_lifecycle_table.png)](http://7xrk8u.com1.z0.glb.clouddn.com/activity_fragment_lifecycle_table.png)

    4. **onStar和onResum、onPause和onStop从描述看起来差不多，对于我们来说有什么实质的不同呢？**

        对于这两对相似的接口，Android系统既然提供了就有其用意：onSatrt和onStop是从activity这个角度来回调的，此时是无法操作的 ; onResume和onPause是从activity是否位于前台来回调，此时由可操作变为不克操作。

    ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%BC%82%E5%B8%B8%E6%83%85%E5%86%B5%E4%B8%8B%E7%9A%84%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F%E5%88%86%E6%9E%90%E5%8F%8A%E4%B8%80%E4%BA%9B%E6%80%9D%E8%80%83%E9%97%AE%E9%A2%98 "异常情况下的生命周期分析及一些思考问题")异常情况下的生命周期分析及一些思考问题

    1. **横竖屏切换时Activity的异常生命周期变化?**

      
```
         Activity创建：
         onCreate-->
         onStart-->
         onResume-->
         ============
         竖屏切横屏：
         onSaveInstanceState-->
         onPause-->
         onStop-->
         onDestroy-->
         onCreate-->
         onStart-->
         onRestoreInstanceState-->
         onResume-->
         ============
         横屏切换竖屏
         onSaveInstanceState-->
        onPause-->
        onStop-->
        onDestroy-->
        onCreate-->
        onStart-->
        onRestoreInstanceState-->
        onResume-->
        onSaveInstanceState-->
        onPause-->
        onStop-->
        onDestroy-->
        onCreate-->
        onStart-->
        onRestoreInstanceState-->
        onResume-->
```
       
    2. **分析**

        * 不设置Activity的android:configChanges时，切屏会重新调用各个生命周期，切横屏时会执行一次，切竖屏时会执行两次

        * 设置Activity的android:configChanges=”orientation”时，切屏还是会重新调用各个生命周期，切横、竖屏时只会执行一次

        * 设置Activity的android:configChanges=”orientation|keyboardHidden”时，切屏不会重新调用各个生命周期，只会执行onConfigurationChanged方法

        * 补充：Activity运行时按下HOME键(跟被完全覆盖是一样的)：
            onSaveInstanceState –> onPause –> onStop;
            重新回到Activity=： onRestart –>onStart—>onResume
            Activity未被完全覆盖只是失去焦点：onPause—>onResume

2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Activity%E6%98%AF%E5%A6%82%E4%BD%95%E4%BF%9D%E5%AD%98%E5%92%8C%E6%81%A2%E5%A4%8DView%E7%9A%84%E7%BB%93%E6%9E%84%EF%BC%9F "Activity是如何保存和恢复View的结构？")Activity是如何保存和恢复View的结构？

    首先，activity被意外终止时，activity会调用onSaveInstanceState()去保存数据，然后activity会委托Window去保存数据，接着window委托它的上级容器去保存数据。顶级容器是一个ViewGroup，一般可能是是DecorView。最后顶级容器再去一一通知子元素去保存数据，对于数据的恢复也是类似的。

3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E8%B5%84%E6%BA%90%E4%B8%8D%E8%B6%B3%E5%AF%BC%E8%87%B4%E4%BD%8E%E4%BC%98%E5%85%88%E7%BA%A7%E7%9A%84activity%E8%A2%AB%E6%9D%80%E6%AD%BB "资源不足导致低优先级的activity被杀死")资源不足导致低优先级的activity被杀死

    activity的优先级：
    1.`前台activity`——正在和用户交互的activity，优先级最高
    2.`可见但非前台activity`—–比如activity中弹出一个对话框，导致activity可见但是位于后台无法与直接交互。
    3.`后台activity`—-已经被暂停的activity，比如执行了onStop(),优先级最低

    对于系统内存不足时，系统会按照优先级来杀死目标activity的进程，然后通过onSaveInstanceState()和onRestoreInstanceState()存储恢复数据。

4. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E9%97%AE%E9%A2%98 "问题")问题

    **假设当前activity为A,若此时用户打开activity B，那么 B 的onResume()和 A 的OnPause()那个先执行？**

    当新启动一个activity时，位于栈顶的旧activity的onPause()会先执行，才会启动新的activity，另外Android对onPause()的解释是不能在其中进行重量级的操作，为了新的activity尽快出现在前台，所以资源回收等重量尽量在onStop()中进行，这也能说明onRsume()先执行。

    > 须知：最开始的两张图展示的 Fragment 与 Activity 的生命周期关系没毛病。
    > onAttach() 和 onCreate() 只在 Fragment 与 Activity 第一次关联时调用。
    > onDestroy() 和 onDetach() 只在 Fragment 的宿主 Activity 销毁时才会被调用。

    **通过 addToBackStack() 将 Fragment 放入回退栈，然后通过 popBackStack() 出栈，Fragment 的生命周期会如何变化呢？**
    所以将 Fragment 通过 addToBackStack() 只涉及 onCreateView() 和 onDestroyView() 这之间的生命周期。add() 和 replace() 不会对 Fragment 的生命周期产生影响，但 add() 方法会造成 Fragment 叠加显示。

    **如果 Fragment 与 ViewPager 结合使用，Fragment 的生命周期又是如何？**
    Fragment 与 ViewPager 结合使用时的只涉及 onCreateView() 和 onDestroyView() 这之间的生命周期生命周期。

    **如果通过 hide() 和 show() 方法来展示隐藏，这时 Fragment 的生命周期又会如何？**
    通过 hide() 、 show() 来隐藏、显示Fragment，此时 Fragment 只改变了可见性，并不涉及生命周期的改变

    **Fragment 和 Activity 的生命周期有关，即：不要在 Fragment 的 onCreate() 方法中操作宿主Activity 的 UI。因为你无法保证此时 宿主Activity 的 UI 已经完全初始化。PS:某些情况下是可以确保 宿主Activity 已经初始化完成的。**

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Android%E7%9A%84%E5%90%AF%E5%8A%A8%E6%A8%A1%E5%BC%8F "Android的启动模式")Android的启动模式

    [![](http://7xrk8u.com1.z0.glb.clouddn.com/activity_launch_mode.png)](http://7xrk8u.com1.z0.glb.clouddn.com/activity_launch_mode.png)

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#activity%E7%9A%84startActivity%E5%92%8Ccontext%E7%9A%84startActivity%E5%8C%BA%E5%88%AB "activity的startActivity和context的startActivity区别")activity的startActivity和context的startActivity区别

    * 从Activity中启动新的Activity时可以直接mContext.startActivity(intent)就好，

    * 如果从其他Context中启动Activity则必须给intent设置Flag:

```
intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK) ;
 mContext.startActivity(intent);
```

   2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Acticity%E5%92%8CFragment%E7%9A%84%E9%80%9A%E4%BF%A1%E6%96%B9%E5%BC%8F "Acticity和Fragment的通信方式")Acticity和Fragment的通信方式

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E8%BF%9B%E7%A8%8B%E9%97%B4%E9%80%9A%E4%BF%A1IPC%E6%9C%BA%E5%88%B6 "进程间通信IPC机制")进程间通信IPC机制

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E4%BB%80%E4%B9%88%E6%98%AFIPC%EF%BC%9F "什么是IPC？")什么是IPC？

    IPC,Inter-Process Communication的缩写，含义为进程通信或者跨进程通信，是指两个进程之间进行数据交换的过程。
    `**线程**：`cup调度的最小单位
    `**进程**：`一般指一个执行单元，在PC和移动设备上指一个程序或者一个应用。
    `**ANR**:`在主线程中进行大量的耗时操作导致界面无法响应。
2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E4%BB%80%E4%B9%88%E6%98%AF%E5%A4%9A%E8%BF%9B%E7%A8%8B%EF%BC%9F "什么是多进程？")什么是多进程？

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%A6%82%E4%BD%95%E5%BC%80%E5%90%AF%E5%A4%9A%E8%BF%9B%E7%A8%8B%EF%BC%9F "如何开启多进程？")如何开启多进程？

    Android中只有一种方式开启多进程，就是在四大组件的啊AndroidMenifest中指定android:process属性。

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%A4%9A%E8%BF%9B%E7%A8%8B%E7%9A%84%E8%BF%90%E8%A1%8C%E6%9C%BA%E5%88%B6%EF%BC%9F "多进程的运行机制？")多进程的运行机制？

    开启多进程造成的问题：

    * 静态成员和单列模式完成失效
    * 线程同步机制失效
    * SharePreferences的可靠性失效
    * Application会多次创建

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E8%BF%9B%E7%A8%8B%E9%9A%94%E7%A6%BB "进程隔离")进程隔离

    为了保证 安全性 & 独立性，一个进程 不能直接操作或者访问另一个进程，即Android的进程是`相互独立`、`隔离的`

3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Android-Binder%E8%B7%A8%E8%BF%9B%E7%A8%8B%E9%80%9A%E4%BF%A1%E7%9A%84%E5%8E%9F%E7%90%86 "Android Binder跨进程通信的原理")Android Binder跨进程通信的原理

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Binder%E6%98%AF%E4%BB%80%E4%B9%88%EF%BC%9F "Binder是什么？")Binder是什么？

    [![](http://7xrk8u.com1.z0.glb.clouddn.com/944365-45db4df339348b9b.png)](http://7xrk8u.com1.z0.glb.clouddn.com/944365-45db4df339348b9b.png)
    **说人话—-**

    * `直观角度`：Binder是Android的一个类，实现了IBinder接口

    * `IPC角度`：Binder是Android一种跨进程通信的方式

    * `Android Framework角度`: Binder是`ServiceManager`连接各 种Manager(`ActivityManager`、`WindowManger`)和相应ManagerService的桥梁

    * `android应用层角度`：Binder是客户端和服务端进行通信的媒介。binderService的时候服务端会返回一个包含服务端业务调用的Binder对象，通过这个binder对象客户端可以获取服务端提供的服务和数据(包含该普通服务和基于AIDL的服务)

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Binder%E8%B7%A8%E8%BF%9B%E7%A8%8B%E9%80%9A%E4%BF%A1%E5%8E%9F%E7%90%86 "Binder跨进程通信原理")Binder跨进程通信原理

    [![通信原理图](http://7xrk8u.com1.z0.glb.clouddn.com/944365-c10d6032f91a103f.png)](http://7xrk8u.com1.z0.glb.clouddn.com/944365-c10d6032f91a103f.png)

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%90%84%E4%B8%AA%E6%A8%A1%E5%9D%97%E7%9A%84%E4%BD%9C%E7%94%A8%EF%BC%9A "各个模块的作用：")各个模块的作用：

    [![各模块的作用](http://7xrk8u.com1.z0.glb.clouddn.com/Binder_module-table.png)](http://7xrk8u.com1.z0.glb.clouddn.com/Binder_module-table.png)

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#ServerManager%E6%98%AF%E5%A6%82%E4%BD%95%E7%AE%A1%E7%90%86server%E7%9A%84%EF%BC%9F "ServerManager是如何管理server的？")ServerManager是如何管理server的？

    * **Binder Client 通过ServerManager获取Binder的引用，binder的引用是由SeverManager转换或者说映射得到的**。

    * **Binder Server在生成一个Binder实体的同时会为其绑定一个名字，并将这个名字封装成一个数据包传递给Binder Driver**；

    * **Binder Driver接收到数据包之后，如果是新的Binder，会为它在内存空间中创建相应的实体节点和一个对实体节点的引用，在源码中中分别对应Binder_node和Binder_ref；创建完后，Binder Driver 会将该引用传递给ServerManager;**

    * **ServerManager接收到之后会从中取出该Binder的名字和引用然后插入到一个数据表中，如此形成了类似DNS中存储的域名到真实IP的映射，然后Binder Client就可利用ServerManager通过名字查询Binder的引用，获取服务**。

    * **当然Binder并非一定在ServerManager中有记录，很多时候Binder Server会将一个Binder实体封装进数据包传递给Binder Client,此时Binder server会在数据包中标注Binder实体的位置，Binder Driver会为该匿名的Binder生成实体节点和引用，并将引用通过映射传递给Binder Clien**

        ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E8%B7%A8%E8%BF%9B%E7%A8%8B%E9%80%9A%E4%BF%A1%E7%9A%84%E6%A0%B8%E5%BF%83%E5%8E%9F%E7%90%86 "跨进程通信的核心原理")跨进程通信的核心原理

    **关于其核心原理：内存映射，具体请看文章：[操作系统：图文详解 内存映射](https://www.jianshu.com/p/719fc4758813)**

    [![Binder Driver](http://7xrk8u.com1.z0.glb.clouddn.com/944365-65a5b17426aed424.png)](http://7xrk8u.com1.z0.glb.clouddn.com/944365-65a5b17426aed424.png)

    ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Binder%E9%80%9A%E4%BF%A1%E5%8E%9F%E7%90%86%E5%9B%BE%EF%BC%9A "Binder通信原理图：")Binder通信原理图：

    [![Binder原理图](http://7xrk8u.com1.z0.glb.clouddn.com/944365-d3c78b193c3e8a38.png)](http://7xrk8u.com1.z0.glb.clouddn.com/944365-d3c78b193c3e8a38.png)

4. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Android%E7%9A%84%E4%B8%ADIPC%E6%96%B9%E5%BC%8F%E5%AF%B9%E6%AF%94 "Android的中IPC方式对比")Android的中IPC方式对比

    [![](http://7xrk8u.com1.z0.glb.clouddn.com/IPC.png)](http://7xrk8u.com1.z0.glb.clouddn.com/IPC.png)

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#View%E7%9A%84%E4%BA%8B%E4%BB%B6%E4%BD%93%E7%B3%BB "View的事件体系")View的事件体系

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#View%E7%9A%84%E6%BB%91%E5%8A%A8 "View的滑动")View的滑动

    * 使用scrollTo/scrollBy
        `scrollTo`:一次性滑动，屏幕左上角坐标为基准
        `scrollBy`:多次的，相对于当前位置的
    * 使用动画
    * 改变布局参数
2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E4%BA%8B%E4%BB%B6%E6%8B%A6%E6%88%AA%E5%92%8C%E5%88%86%E5%8F%91 "事件拦截和分发")事件拦截和分发

    [![](http://7xrk8u.com1.z0.glb.clouddn.com/android_view_hierarchy.jpg)](http://7xrk8u.com1.z0.glb.clouddn.com/android_view_hierarchy.jpg)
    **先来一段伪代码简单表示事件的传递规则**

    

    |  
    1
    2
    3
    4
    5
    6
    7
    8
    9
    10
    11
    12
    13
    14
    15
    

     |  
    public boolean dispatchTouchEvent(MotionEvent ev){
     boolean cousume = flase;
     //拦截
     if(onInterceptTouchEvent(ev)){
     //消费
     consume = onTouchEvent();
     }else{
     //子view继续分发事件
     consume = child.dispatchTouchEvent(ev);
     }
     return consume;
    }
    
    MotionEvent=====>Activty----->Window(PhoneWindow)------->DecorView---->
    --->顶级View(setContentView()，一般是ViewGroup)------->目标View
    

     |

    

    > **说人话描述上面这段代码**：

    **点击事件产生后，经过Activty,PhoneWindow,DecorView的分发传递，事件会传递给顶级View(一般是ViewGroup)，这时候它的dispatchTouchEvent()方法会被调用，
    如果内部的onInterceptTouchEvent()方法返回true,则表示它拦截当前的事件，特殊的如果ViewGroup的mOnTouchListener被设置，事件会被onTouch()处理，否则事件就正常交给ViewGroup的onTouchEvent()方法会处理；
    相反，如果ViewGroup的onInterceptTouchEvent()方法返回出false,则表示不拦截当前的事件，这时当前的事件就会传递它的子元素，接着子元素的dispatchTouchEvent()方法会被调用，如此反复直到事件被处理。**

    [![](http://7xrk8u.com1.z0.glb.clouddn.com/motion_event_method.png)](http://7xrk8u.com1.z0.glb.clouddn.com/motion_event_method.png)

3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#View%E7%9A%84%E6%BB%91%E5%8A%A8%E5%86%B2%E7%AA%81 "View的滑动冲突")View的滑动冲突

    * > **解决ScrollView嵌套ListView和GridView冲突的方法**

        重写ListView的onMeasure方法，来自定义高度：

```
        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
         int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
         super.onMeasure(widthMeasureSpec, expandSpec);
        }

```        
        主要考察对MeasureSpec的三种模式的理解,相关文章.

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#View%E7%9A%84%E5%B7%A5%E4%BD%9C%E5%8E%9F%E7%90%86 "View的工作原理")View的工作原理

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#View%E7%9A%84%E7%BB%98%E5%88%B6%E6%B5%81%E7%A8%8B%EF%BC%9A "View的绘制流程：")View的绘制流程：

    View的绘制是从ViewRoot的performTraversals()方法开始的，该方法会依次调用performMeasure、performLayout、performDraw三个方法，这三个方法依次完成顶级view的measure、layout、draw。其中perform会调用measure方法，在measure中有会调用onMeasure方法，在onMeasure方法中会对所有的子view进行measure，此时整个measure流程从父容器传到了子元素，完成一次measur过程，接着子元素会重复父容器的measure过程，如此反复完成整个view树的遍历。同理performLayout、performDraw与performMeasure的流程是类似的，最终完成view的绘制。

2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#view%E7%9A%84%E5%B7%A5%E4%BD%9C%E6%B5%81%E7%A8%8B%EF%BC%9A "view的工作流程：")view的工作流程：

    主要是指measure、layout、draw这三大流程，其中measure确定view的测量宽/高；layout确定view的最终宽/高和四个顶点的位置；二draw将view绘制在屏幕上。

    对于view直接调用onMeasure方法来测量，而ViewGroup是抽象类，没有重写onMeasure方法，而是通过measureChildren遍历子元素，通过measureChild方法调用子元素的measure完成测量；

    ViewGroup layout的大致流程：通过setFrame方法来设定view的四个顶点的位置，view的四个顶点一单确定view在父容器的中的位置也就确定，接着调用onLaytout确定子元素的位置;

    * Draw遵循步骤：
    * 1、绘制背景background.draw(canvas);
    * 2、绘制自己onDraw;
    * 3、绘制children–dispatchDraw;
    * 4、绘制装饰—onDrawScrollBars.
3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E8%87%AA%E5%AE%9A%E4%B9%89View "自定义View")自定义View

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E8%87%AA%E5%AE%9A%E4%B9%89View%E7%9A%84%E5%88%86%E7%B1%BB%E5%8F%8A%E6%B5%81%E7%A8%8B%EF%BC%9A "自定义View的分类及流程：")自定义View的分类及流程：

    * 1、**继承View重写onDraw方法**；需要自己支持warp_content，处理padding；

    * 2、**继承ViewGroup派生特殊的布局**，方法需要适当的处理ViewGroup的测量和布局的两个过程，并同时处理子元素的布局和测量过程。

    * 3、**继承特定的View**，如TextView，对已有的功能进行拓展，无序自己支持warp_content，处理padding

    * 4、**继承特定的ViewGroup**,如线性布局，此方法不需要自己处理ViewGroup的测量和布局。

    * 5、**为了使自定义的View更易用，还需要添加自定义属性，以便在布局文件中使用：**

        * 1、在values目录中创建自定义属性xml文件，如attrs.在values目录中创建自定义属性xml文件
        * 2、在view的构造方法中解析自定义的属性，并相应的处理，如定义默认值等。
        * 3、在布局文件中声明然后使用。

        ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E7%9B%B4%E6%8E%A5%E7%BB%A7%E6%89%BFview%E6%88%96%E8%80%85ViewGroup%E7%9A%84%E6%8E%A7%E4%BB%B6%EF%BC%8C%E6%9C%89%E6%97%B6%E5%80%99warp-content%E8%BE%BE%E4%B8%8D%E5%88%B0%E9%A2%84%E6%9C%9F%E6%95%88%E6%9E%9C%EF%BC%9F "直接继承view或者ViewGroup的控件，有时候warp_content达不到预期效果？")直接继承view或者ViewGroup的控件，有时候warp_content达不到预期效果？

        `原因`：当view在布局中使用warp_content，此时specMode为AT_MOST，宽高等于specSize，这种情况下View的specSize为parentSize,而parentSized的大小为父容器当前的剩余空间，使得和使用match_parent效果一致。
        `解决方法`：给View设定默认的宽/高，并在warp_content时根据情况设置此宽/高，即可。

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E8%87%AA%E5%AE%9A%E4%B9%89View%E6%B3%A8%E6%84%8F%E4%BA%8B%E9%A1%B9 "自定义View注意事项")自定义View注意事项

    * 1.让View支持warp_content

    * 2.如有必要，让view支持padding

    * 3.尽量不要在view中使用Handler,没必要，view本身提供post方法，完全可以替代Handler的作用，除非明确用handler来发送消息。

    * View中如果有线程或者动画，需要及时停止，View的OnDetachFromWindow是个好时机，当包含此view的Activity退出或者当前的View被remove是，此方法都会被调用。同事view背的不可见时候也需停止动画或者线程，避免内存泄漏。

    * View带有嵌套滑动的情形是，需要处理滑动冲突。

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Android%E5%8A%A8%E7%94%BB%E5%88%86%E6%9E%90 "Android动画分析")Android动画分析

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Android%E5%8A%A8%E7%94%BB%E5%88%86%E7%B1%BB%EF%BC%9Aview%E5%8A%A8%E7%94%BB%EF%BC%8C%E5%B1%9E%E6%80%A7%E5%8A%A8%E7%94%BB%E5%92%8C%E6%96%B0%E5%A2%9E%E7%9A%84%E7%89%A9%E7%90%86%E5%8A%A8%E7%94%BB "Android动画分类：view动画，属性动画和新增的物理动画")Android动画分类：view动画，属性动画和新增的物理动画

    * **View动画**：对场景里的对象不断做图像变换（平移，缩放，旋转，透明度）从而产生一系列的动画效果，是一种渐进式动画，支持自定义；它包含该两个特殊的类型帧动画和补间动画：

        * 帧动画：通过顺序播放一系列图像从而产生动画效果，可以简单理解为图片的切换动画。图片过多过大会导致oom;
        * 补间动画：是帧动画的特殊形式，只需要提供开始图片和结束图片作为开始帧和结束帧，而动画的中间帧由系统计算补充。
    * **属性动画**：通过动态的改变对象的属性从而达到动画效果。

    * **物理动画**：基于物理规律的动画效果。
2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E8%87%AA%E5%AE%9A%E4%B9%89view%E5%8A%A8%E7%94%BB%E7%9A%84%E5%8F%8A%E5%85%B6%E4%BD%BF%E7%94%A8%E5%9C%BA%E6%99%AF "自定义view动画的及其使用场景")自定义view动画的及其使用场景

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E8%87%AA%E5%AE%9A%E4%B9%89view%E5%8A%A8%E7%94%BB "自定义view动画")自定义view动画

    继承Animation抽象类—->重写initialize和applyTransformation方法—–>在initialize中做初始化工作，在applyTransformation中进行相应的矩阵变换。
    * `view动画的特殊使用场景`：在ViewGroup中控制元素的出场效果，实现activity的出场动画。
3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%B1%9E%E6%80%A7%E5%8A%A8%E7%94%BB%EF%BC%9A "属性动画：")属性动画：

    常见的几个动画类：ValueAnimator,ObjectAnimator,AnimatorSet。
    `插值器`：根据时间流逝的百分比来计算出当前属性值改变的百分比
    `估值器`:根据当前属性变化的百分比计算改变后的属性值

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%B1%9E%E6%80%A7%E5%8A%A8%E7%94%BB%E7%9A%84%E5%B7%A5%E4%BD%9C%E5%8E%9F%E7%90%86%EF%BC%9A "属性动画的工作原理：")属性动画的工作原理：

    在一定时间间隔内和值范围内，通过不断对值进行改变，并不断将该值赋给对象的属性，从而实现该对象在该属性上的动画效果。因此要求作用的对象有该属性的set方法，如果没有设置初始值，还需要对象提供get方法，或者进行包装设置getter/setter方法。ValueAnimator不断控制值的变化，然后手动赋值给对象的属性，ObjectAnimator直接操作对象属性的值。

4. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E4%BD%BF%E7%94%A8%E5%8A%A8%E7%94%BB%E7%9A%84%E6%B3%A8%E6%84%8F%E4%BA%8B%E9%A1%B9%EF%BC%9A "使用动画的注意事项：")使用动画的注意事项：

    1、帧动画中如果使用的图片过多多大容易出现OOM;
    2、内存泄漏：属性动画中的无限循环动画需要在activity退出时及时停止，避免activity无法释放造成内存泄漏。
    3、尽量使用dp保证在不同设备上的效果；
    4、动画元素的交互问题：使用view动画移动之后点击事件还在原地，点击移动后的view无效======这是因为view动画只是对控件影像的操作，到达动画视觉的效果，而控件还留在原来的区域。

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E7%90%86%E8%A7%A3Window%E5%92%8CWinowManager "理解Window和WinowManager")理解Window和WinowManager

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E6%A6%82%E8%BF%B0%EF%BC%9A "概述：")概述：

    Window表示一个窗口的概念，是一个抽象类，Android的所有视图都通过Window呈现，并依附于它，不管是activity、dialog、还是toast，Window都是view的直接管理者。具体实现是PhoneWindow，通过WindowManager创建window，window的外界访问入口是WindowMananger，具体实现在WindowManagerServer中，因此windowmanger和WindowManagerServer的交互式IPC过程。

2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Window%E7%9A%84%E5%86%85%E9%83%A8%E6%9C%BA%E5%88%B6 "Window的内部机制")Window的内部机制

    [![](/images/Window的内部机制.jpg)](/images/Window的内部机制.jpg)

3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#PhoneWindow%E5%AE%9E%E4%BE%8B%E6%98%AF%E5%9C%A8%E5%93%AA%E4%B8%80%E4%B8%AA%E7%B1%BB%E5%93%AA%E4%B8%80%E4%B8%AA%E6%96%B9%E6%B3%95%E4%B8%AD%E5%AE%9E%E4%BE%8B%E5%8C%96%E7%9A%84%EF%BC%9F%E4%B9%9F%E5%B0%B1%E6%98%AFWinodow%E7%9A%84%E5%88%9B%E5%BB%BA%E8%BF%87%E7%A8%8B%EF%BC%9F "PhoneWindow实例是在哪一个类哪一个方法中实例化的？也就是Winodow的创建过程？")PhoneWindow实例是在哪一个类哪一个方法中实例化的？也就是Winodow的创建过程？

    以Activity中Window创建为例：在activity的实例创建之后会调用Activity的attach方法，此方法中系统会创建Activity所属的Window对象，Window对象的创建是通过PolicyManager的具体实现类Policy类中的makeNewWindow方法实现的，此方法中放返回一个PhoneWindow的实例。因此此过程就是PhoneWindow的实例化过程，而PhoneWindow是Window的具体实现类。
4. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Activity%E7%9A%84%E8%A7%86%E5%9B%BE%E5%88%9B%E5%BB%BA%E8%BF%87%E7%A8%8B "Activity的视图创建过程")Activity的视图创建过程

    1. 创建Window:Activity的attach方法中通过PolicyManager的makeNewWindow方法创建实现类PhoneWindow;

    2. 初始化DecorView并将定义的布局添加到DecorView的`mParentContent`中，回调；

    3. DecorView已经初始化完毕，ActivityThread的handleResumeActivity方法调用onResume方法，然后在makeVisible()方法中使activity视图可见。

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%9B%9B%E5%A4%A7%E7%BB%84%E4%BB%B6%E7%9A%84%E5%B7%A5%E4%BD%9C%E8%BF%87%E7%A8%8B "四大组件的工作过程")四大组件的工作过程

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E6%A6%82%E8%BF%B0 "概述")概述

    **Activity**：展示型组件，向用户展示界面，接收用户输入进行交互；
    **Service**:计算性组件，在后台执行一系列计算任务；
    **BroadcastReceiver**:消息型组件，应用在不同组件乃至不同应用之间传递消息。可实现低耦合观察者模式；
    **ContentProvider**:数据共享型组件，用于向其他组件乃至应用间共享数据。

2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Activity%E7%9A%84%E5%B7%A5%E4%BD%9C%E8%BF%87%E7%A8%8B "Activity的工作过程")Activity的工作过程

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#activity%E6%98%AF%E5%A6%82%E4%BD%95%E5%90%AF%E5%8A%A8%EF%BC%9F "activity是如何启动？")activity是如何启动？

    [![](/images/activity启动流程.jpg)](/images/activity启动流程.jpg)

3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Service%E7%9A%84%E5%B7%A5%E4%BD%9C%E8%BF%87%E7%A8%8B "Service的工作过程")Service的工作过程

    [![](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/service_lifecycle.jpg)](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/service_lifecycle.jpg)

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Service%E7%9A%84%E5%90%AF%E5%8A%A8%E8%BF%87%E7%A8%8B%E5%92%8C%E7%BB%91%E5%AE%9A%E8%BF%87%E7%A8%8B "Service的启动过程和绑定过程")Service的启动过程和绑定过程

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%A6%82%E4%BD%95%E4%BF%9D%E8%AF%81Service%E4%B8%8D%E8%A2%AB%E6%9D%80%E6%AD%BB%EF%BC%9F "如何保证Service不被杀死？")如何保证Service不被杀死？

    * 1.onStartCommand方法，返回START_STICKY
    * 2.提升service优先级
    * 3.提升service进程优先级
    * 4.onDestroy方法里重启service
    * 5.Application加上Persistent属性
    * 6.双进程保护,两个Service相互唤醒
    * 7.使用JobService(5.0之后)

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E6%80%8E%E4%B9%88%E5%9C%A8Service%E4%B8%AD%E5%88%9B%E5%BB%BADialog%E5%AF%B9%E8%AF%9D%E6%A1%86 "怎么在Service中创建Dialog对话框")怎么在Service中创建Dialog对话框

    1. **在Service中创建**

        * 1.构建Dialog对象之后，在show()方法之前设置Dialog 的Window类型为系统警告弹框
            `mDialog.getWindow.setType(WindowManger.LayoutParams.TYPE_SYSYTEM_ALERT)`

        * 2.在AndroidManifest清单文件的申明对应的权限`android.permission.SYSTEM_ALERT_WINDOW`

        * 3.对于6.0以上的版本还需要申请运行时权限：在activity的onResume中判断是否允许绘制叠加层– `Settings.canDrawOverlays()`，没有则请求。

        [![](http://7xrk8u.com1.z0.glb.clouddn.com/dialog.png)](http://7xrk8u.com1.z0.glb.clouddn.com/dialog.png)

    2. **在Service中发送广播在Activity中显示**

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Android%E7%9A%84%E6%B6%88%E6%81%AF%E6%9C%BA%E5%88%B6 "Android的消息机制")Android的消息机制

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E6%A6%82%E8%BF%B0%EF%BC%9A-1 "概述：")概述：

    android的消息机制是指Handler的运行机制以及Handler所附带的MessageQueue和Looperd的工作过程，三者是一个整体。Handler 的主要任务是将任务切换到某个指定的线程去执行。
2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E6%B6%88%E6%81%AF%E6%9C%BA%E5%88%B6%E5%8E%9F%E7%90%86 "消息机制原理")消息机制原理

    `message:`包含事件的信息以及消息的处理对象,message.next()指向下一个可用的message
    `MessageQueue:`一组以单链表形式维护的待处理的消息；
    `Looper:`消息循环
    `Handler:`消息投递、处理器。
    [![](/images/Handler消息机制.jpg)](/images/Handler消息机制.jpg)
    **讲人话====>整个机制就像工厂的生成线，Message是待处理的产品，MessageQueu是传送带，Looper是发动机，而Handler则是工人。工人在生产线一端Thread 1投送待处理的产品Message到传送带MessageQueue上，经过发送机Looper的不停循环转动传送到生产线另一端Thread 2,然后工人拿出产品做相应的处理。**
3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#ThreadLocal "ThreadLocal")ThreadLocal

    ThreadLocal是线程内部的数据存储类，通过它可以在制定的线程中存储数据，数据存储之后只能在指定的线程中获取到存储的数据，对于其他线程来说则无法获取到。
    `使用时机`：当某些数据是以线程为作用域，并且不同线程具有不同的数据副本时。比如,线程的Looper,作用域为当前线程且不线程有不同的Looper;全局的监听器对象。
4. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%9C%A8%E4%B8%8D%E4%BC%A0%E9%80%92Looperd%E5%8F%82%E6%95%B0%E7%BB%99Handler%E6%9E%84%E9%80%A0%E5%87%BD%E6%95%B0%E7%9A%84%E6%83%85%E5%86%B5%E4%B8%8B%EF%BC%8C%E6%9B%B4%E6%96%B0UI%E7%9A%84Handler%E4%B8%BA%E4%BB%80%E4%B9%88%E5%BF%85%E9%A1%BB%E5%9C%A8UI%E7%BA%BF%E7%A8%8B%E4%B8%AD%E5%88%9B%E5%BB%BA%EF%BC%9F "在不传递Looperd参数给Handler构造函数的情况下，更新UI的Handler为什么必须在UI线程中创建？")在不传递Looperd参数给Handler构造函数的情况下，更新UI的Handler为什么必须在UI线程中创建？

    因为每个Handler会关联一个消息列队，而消息列队被封装在Looper中，但是每个Looper又是存储在ThreadLocal中的，基于ThreadLocal的特性，也就是说每个消息列队只属于一个线程。因此，如果Looper在线程A中创建，那么Looper只能被线程A访问。不带参数的Handler默认获取了UI线程中的MianLooper,作为消息投递，处理的Handler，要想访问MainLooper所在线程中的消息列队，必须在与之相同的线程中，即UI线程中创建。
5. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%8F%AA%E8%83%BD%E5%9C%A8%E4%B8%BB%E7%BA%BF%E7%A8%8B%E4%B8%AD%E6%9B%B4%E6%96%B0UI%EF%BC%8C%E8%BF%99%E7%A7%8D%E8%AF%B4%E6%B3%95%E5%AF%B9%E5%90%97%EF%BC%9F "只能在主线程中更新UI，这种说法对吗？")只能在主线程中更新UI，这种说法对吗？

    不对，应该说只能在创建UI的线程中更新UI
6. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Handler%E5%AF%BC%E8%87%B4%E7%9A%84%E5%86%85%E5%AD%98%E6%B3%84%E6%BC%8F "Handler导致的内存泄漏")Handler导致的内存泄漏

    造成泄漏的诱因有两个：1、主线程中的Looper对象的生命周期==应用的生命周期；2、在java中菲静态内部类和匿名内部类默认持有外部类的引用。
    由于Looper内部消息队列中的Message默认包含了对处理者handler的引用(msg.traget=handler)，而handler实例默认持有外部类，也就是activity的引用，如果在activity结束时，还有未处理的消息或者正在处理消息，使得引用关系继续保持，从而导致外部类无法被销毁回收，造成内存泄漏。
    **解决方法：**

    * 1.外部类结束生命周期时清空handler内的消息列队，比如在onDestory中调用 mHandler.removeCallbacksAndMessages(null);

    * 2.将handler设置成静态内部类并对外部类进行弱引用。

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E4%B8%BA%E4%BB%80%E4%B9%88%E4%B8%80%E4%B8%AA%E7%BA%BF%E7%A8%8B%E5%8F%AA%E6%9C%89%E4%B8%80%E4%B8%AALooper%E3%80%81%E5%8F%AA%E6%9C%89%E4%B8%80%E4%B8%AAMessageQueue%EF%BC%9F "为什么一个线程只有一个Looper、只有一个MessageQueue？")为什么一个线程只有一个Looper、只有一个MessageQueue？

    ThreadLoca这个线程变量来存储Looper，looper 中实例化MessageQueue，在创建Looper的Looper.prepare()方法中，如果ThreadLocal中已经存在looper会报错


``` 
     public static void prepare() {
     prepare(true);
    }
    
    private static void prepare(boolean quitAllowed) {
     // 这里是关键，如果这个线程已经存在Looper报异常
     if (sThreadLocal.get() != null) {
     throw new RuntimeException("Only one Looper may be created per thread");
     }
     // 不存在，创建一个Looper设置到sThreadLocal
     sThreadLocal.set(new Looper(quitAllowed));
    }
```
   
#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Android%E7%9A%84%E7%BA%BF%E7%A8%8B%E5%92%8C%E7%BA%BF%E7%A8%8B%E6%B1%A0 "Android的线程和线程池")Android的线程和线程池

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E4%B8%BB%E7%BA%BF%E7%A8%8B%E5%92%8C%E5%AD%90%E7%BA%BF%E7%A8%8B "主线程和子线程")主线程和子线程

    进程所拥有的的线程叫做主线程，除主线程之外的线程叫做子线程，或工作线程。
    主线程主要用来是运行四大组件以及处理界面交互相关的逻辑，子线程用来执行耗时操作。
2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Android%E4%B8%AD%E7%9A%84%E7%BA%BF%E7%A8%8B%E5%BD%A2%E6%80%81 "Android中的线程形态")Android中的线程形态

    1. 传统的Thread
    2. AsyncTask
    3. HandlerThread
    4. IntentService
3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#HandlerThread%E3%80%81IntentService%E7%90%86%E8%A7%A3 "HandlerThread、IntentService理解")HandlerThread、IntentService理解

    《android开发艺术探索》第十一章

4. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#android%E4%B8%AD%E7%9A%84%E7%BA%BF%E7%A8%8B%E6%B1%A0 "android中的线程池")android中的线程池

    **线程池的优点**
    * 重用线程池中线程，避免线程创建和销毁带来的性能开销
    * 能有效控制线程的最大并发数，避免大量线程之间因相互枪战系统资源而导致阻塞。
    * 能够对线程进行简单的控制
5. **线程池的分类**
    * FixedThreadPool :固定线程数，空闲状态不会被回收
    * CachedThreadPool : 线程数量不固定，只有非核心线程，闲置状态是被停止
    * ScheduledThreadPool :核心线程固定，非核心线程不固定，闲置回收，执行定时或者周期任务
    * SingleThreadPool :只有一个核心线程，任务在同一个线程中顺序执行
6. **线程池的参数详解**
    ？？？？

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Bitmap%E7%9A%84%E5%8A%A0%E8%BD%BD%E5%92%8C%E7%BC%93%E5%AD%98 "Bitmap的加载和缓存")Bitmap的加载和缓存

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Bitmap%E7%9A%84%E9%AB%98%E6%95%88%E5%8A%A0%E8%BD%BD "Bitmap的高效加载")Bitmap的高效加载

    核心思想是：采用BitmapFactory.Option设置采样率inSampleSzie来加载所需尺寸的图片。
    获取采样率的流程：

    1. 将BitmapFactory.Option的inJustDecodeBounds参数为true并加载图片;
    2. 从BitmapFactory.Option中解析出图片的原始宽高，它们对应于outWidth和outHeight;
    3. 根据采样率的规则、结合目标view的大小计算出采样率inSampleSize;
    4. 将BitmapFactoory.Option的inJustDecodeBounds设置为false,重新加载图片。
2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E9%AB%98%E6%95%88%E5%8A%A0%E8%BD%BD%E5%A4%A7%E5%9B%BE "高效加载大图")高效加载大图

    [Android 高清加载巨图方案 拒绝压缩图片](https://blog.csdn.net/lmj623565791/article/details/49300989)

3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#android%E7%9A%84%E7%BC%93%E5%AD%98%E7%AD%96%E7%95%A5 "android的缓存策略")android的缓存策略

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#android%E5%9B%BE%E7%89%87%E7%9A%84%E4%B8%89%E7%BA%A7%E7%BC%93%E5%AD%98 "android图片的三级缓存")android图片的三级缓存

    1. 首次加载的时候通过网络加载，获取图片，然后保存到内存和 SD 卡中；

    2. 之后运行 APP 时，优先访问内存中的图片缓存；

    3. 如果内存中没有，则加载本地 SD 卡中的图片,如果内存中没有最后再从网络获取。

        **具体的缓存策略可以是这样的：内存作为一级缓存，本地作为二级缓存，网络加载为最后。其中，内存使用 LruCache ，其内部通过 LinkedhashMap 以强引用的方式存储外界的缓存对象，并使用Lru最近最少算法维护链表；对于本地缓存，使用 DiskLruCache。加载图片的时候，首先使用 LRU 方式进行寻找，若找不到指定内容，按照三级缓存的方式，进行本地搜索，还没有就网络加载。**

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Android%E7%9A%84%E6%80%A7%E8%83%BD%E4%BC%98%E5%8C%96 "Android的性能优化")Android的性能优化

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%B8%83%E5%B1%80%E4%BC%98%E5%8C%96 "布局优化")布局优化

2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%86%85%E5%AD%98%E6%B3%84%E9%9C%B2%E4%BC%98%E5%8C%96 "内存泄露优化")内存泄露优化

    [Android之内存泄漏以及解决办法](https://blog.csdn.net/qq_16628781/article/details/67761590)
3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E7%BB%98%E5%88%B6%E4%BC%98%E5%8C%96 "绘制优化")绘制优化

4. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E7%BA%BF%E7%A8%8B%E4%BC%98%E5%8C%96 "线程优化")线程优化

5. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%93%8D%E5%BA%94%E9%80%9F%E5%BA%A6%E4%BC%98%E5%8C%96%E5%92%8CANR%E6%97%A5%E5%BF%97%E5%88%86%E6%9E%90 "响应速度优化和ANR日志分析")响应速度优化和ANR日志分析

6. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#ListView%E5%92%8CBitmap%E4%BC%98%E5%8C%96 "ListView和Bitmap优化")ListView和Bitmap优化

    具体详见《Android开发艺术探索》第十五章

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#TCP%E5%92%8CUDP "TCP和UDP")TCP和UDP

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E7%AE%80%E8%BF%B0TCP%E5%BB%BA%E7%AB%8B%E8%BF%9E%E6%8E%A5%E7%9A%84%E4%B8%89%E6%AC%A1%E6%8F%A1%E6%89%8B%E5%8F%8A%E5%8E%9F%E5%9B%A0%EF%BC%9F "简述TCP建立连接的三次握手及原因？")简述TCP建立连接的三次握手及原因？

    [![](http://7xrk8u.com1.z0.glb.clouddn.com/TCP.jpg)](http://7xrk8u.com1.z0.glb.clouddn.com/TCP.jpg)
    **ACK(Acknowledgement)**： 确认字符。TCP协议规定，只有ACK=1时有效，也规定连接建立后所有发送的报文的ACK必须为1
    **SYN(Synchronization)** ： 在连接建立时用来同步序号。当SYN=1而ACK=0时，表明这是一个连接请求报文。对方若同意建立连接，则应在响应报文中使SYN=1和ACK=1\. 因此, SYN置1就表示这是一个连接请求或连接接受报文。
    **FIN（finis）**即完，终结的意思， 用来释放一个连接。当 FIN = 1 时，表明此报文段的发送方的数据已经发送完毕，并要求释放连接。
    **步骤：**

    * 1、**首先由Client发出请求连接报文,即 SYN=1+ACK=0,声明自己的序号是 seq=x**
    * 2、**然后Server 进行回复确认，即 SYN=1，声明自己的序号是 seq=y,并设置为ack=x+1,**
    * 3、**最后Client 再进行一次确认， seq=x+1, ack=y+1。收到确认之后连接建立完毕，可以传输数据**

        [![](http://7xrk8u.com1.z0.glb.clouddn.com/TCP_shake_hands_LI.jpg)](http://7xrk8u.com1.z0.glb.clouddn.com/TCP_shake_hands_LI.jpg)
        **原因：**

        > 防止服务器接收到早已失效的连接请求，从而一直等待客户端的请求，导致形成锁死、浪费资源

    假设不采用“三次握手”，即服务器发出确认报文后，TCP连接就建立起来，但是客户端并没有继续发出建立连接的请求报文，因此不会给服务器发送数据，对于客户端来说，该报文已经失效，但是服务器以为TCP连接已建立，然后一直等待客户端发送数据，会形成锁死。
    **就好比**：

    > 老板只是查个岗，于是在群里随便问：小明在吗？

    > 然后就看到了小明迅速的恢复：在的,老板！

    > 老板心想：果然没看错小明！然后老板忙自己的去了。

    这时候要是小明是个二货（不进行三次握手），以为老本板有什么任务交给他，就傻傻的等着，等了一天老板也没再说话。这就是不进行三次握手的结果—锁死、浪费资源。

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#TCP%E5%9B%9B%E6%AC%A1%E5%88%86%E6%89%8B "TCP四次分手")TCP四次分手

    [![](http://7xrk8u.com1.z0.glb.clouddn.com/TCP_wave_hands.png)](http://7xrk8u.com1.z0.glb.clouddn.com/TCP_wave_hands.png)

    > **解析：**

    假设老板和小明都在加班，一起加班，共同加班——>`数据双向传输`
    老板一看表两点了啊，于是
    `数据单向传输`
    老板：小明你的模块提交了没有，提交了早点回去休息，明天还上班呢！

    ```
    客户端|终止等待1

    ```

    小明：还有一点，马上就好!

    ```
    服务器|关闭等待
    客户端|终止等待2

    ```

    转眼三点了，小明也完成了工具，于是：
    小明：老板，醒醒，醒醒，我的模块提交了！

    ```
    服务器|最终确认

    ```

    老板：额，什么，好，我在看一下，你可以走了！明天不要迟到！
    然后，小明拖着疲倦的身体回了；

    ```
    服务器|关闭

    ```

    过了没多久—-等小明真的走了

    ```
    客户端|时间等待

    ```

    老板也收拾东西，开车回家了。

    ```
    客户端|关闭

    ```

2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E4%B8%BA%E4%BB%80%E4%B9%88%E5%AE%A2%E6%88%B7%E7%AB%AF%E5%85%B3%E9%97%AD%E8%BF%9E%E6%8E%A5%E5%89%8D%E8%A6%81%E7%AD%89%E5%BE%852MSL%E6%97%B6%E9%97%B4%EF%BC%9F "为什么客户端关闭连接前要等待2MSL时间？")为什么客户端关闭连接前要等待2MSL时间？

    `MSL`–最长报文寿命（Maximum Segment Lifetime），报文在网络中存活的事件。

    * 原因1：为了保证客户端发送的最后1个连接释放确认报文能到达服务器，从而使得服务器能正常释放连接；

    * 原因2：防止客户端已失效的连接请求报文出现在连接中。客户端发送了最后1个连接释放请求确认报文后，再经过2MSL时间，则可使本连接持续时间内所产生的所有报文段都从网络中消失。

3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#DDoS%E6%94%BB%E5%87%BB "DDoS攻击")DDoS攻击

    DDOS全名是Distribution Denial of service (分布式拒绝服务攻击),很多DOS攻击源一起攻击某台服务器就组成了DDOS攻击。DoS的攻击方式有很多种，最基本的DoS攻击就是利用合理的服务请求来占用过多的服务资源，从而使服务器无法处理合法用户的指令。

    **未连接队列**：在三次握手协议中，服务器维护一个未连接队列，该队列为每个客户端的SYN包（syn=j）开设一个条目，该条目表明服务器已收到SYN包，并向客户发出确认，正在等待客户的确认包。这些条目所标识的连接在服务器处于Syn_RECV状态，当服务器收到客户的确认包时，删除该条目，服务器进入ESTABLISHED状态。

    **Backlog参数**：表示未连接队列的最大容纳数目。

    **SYN-ACK重传次数** 服务器发送完SYN－ACK包，如果未收到客户确认包，服务器进行首次重传，等待一段时间仍未收到客户确认包，进行第二次重传，如果重传次数超过系统规定的最大重传次数，系统将该连接信息从半连接队列中删除。注意，每次重传等待的时间不一定相同。

    **半连接存活时间**：是指半连接队列的条目存活的最长时间，也即服务从收到SYN包到确认这个报文无效的最长时间，该时间值是所有重传请求包的最长等待时间总和。有时我们也称半连接存活时间为Timeout时间、SYN_RECV存活时间。

    假设一个用户向服务器发送了SYN报文后突然死机或掉线，那么服务器在发出SYN+ACK应答报文后是无法收到客户端的ACK报文的（第三次握手无法完成），这种情况下服务器端一般会重试（再次发送SYN+ACK给客户端）并等待一段时间后丢弃这个未完成的连接，这段时间的长度我们称为SYN Timeout，一般来说这个时间是分钟的数量级（大约为30秒-2分钟）；一个用户出现异常导致服务器的一个线程等待1分钟并不是什么很大的问题，但如果有一个恶意的攻击者大量模拟这种情况，服务器端将为了维护一个非常大的半连接列表而消耗非常多的资源—-数以万计的半连接，即使是简单的保存并遍历也会消耗非常多的CPU时间和内存，何况还要不断对这个列表中的IP进行SYN+ACK的重试。实际上如果服务器的TCP/IP栈不够强大，最后的结果往往是堆栈溢出崩溃—即使服务器端的系统足够强大，服务器端也将忙于处理攻击者伪造的TCP连接请求而无暇理睬客户的正常请求（毕竟客户端的正常请求比率非常之小），此时从正常客户的角度看来，服务器失去响应，这种情况我们称做：服务器端受到了SYN Flood攻击（SYN洪水攻击）

    **在服务端返回一个确认的SYN-ACK包的时候有个潜在的弊端，如果发起的客户是一个不存在的客户端，那么服务端就不会接到客户端回应的ACK包。
    这时服务端需要耗费一定的数量的系统内存来等待这个未决的连接，直到等待超时关闭，才能施放内存。
    如果恶意者通过ip欺骗，发送大量SYN包给受害者系统，导致服务端存在大量未决的连接并占用大量内存和tcp连接，从而导致正常客户端无法访问服务端，这就是SYN洪水攻击的过程。**

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Http-Https "Http/Https")Http/Https

详情见[HTTP与HTTPS有什么区别](http://www.yyglike.com/2018/04/03/Android%E7%BD%91%E7%BB%9C%E7%BC%96%E7%A8%8B%E9%9D%A2%E8%AF%95%E9%A2%98%E9%9B%86/#HTTP%E4%B8%8EHTTPS%E6%9C%89%E4%BB%80%E4%B9%88%E5%8C%BA%E5%88%AB%EF%BC%9F)

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Java%E5%9E%83%E5%9C%BE%E5%9B%9E%E6%94%B6%E6%9C%BA%E5%88%B6 "Java垃圾回收机制")Java垃圾回收机制

详情见[Java面试题集](http://www.yyglike.com/2018/04/03/Java%E9%9D%A2%E8%AF%95%E9%A2%98%E9%9B%86/)

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#JVM "JVM")JVM

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E7%B1%BB%E5%8A%A0%E8%BD%BD%E6%9C%BA%E5%88%B6 "类加载机制")类加载机制

详情见[Java面试题集](http://www.yyglike.com/2018/04/03/Java%E9%9D%A2%E8%AF%95%E9%A2%98%E9%9B%86/)

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Java%E7%9A%84%E5%9B%9B%E7%A7%8D%E5%BC%95%E7%94%A8%E7%B1%BB%E5%9E%8B "Java的四种引用类型")Java的四种引用类型

1. **强引用**
    Java中所有new出来的对象都是强引用类型，回收的时候，GC宁愿抛出OOM异常，也不回收它。
2. **软引用，SoftReference**
    内存足够时，不回收。内存不够时，就回收。
3. **弱引用，WeakReference**
    GC一出来工作就回收它。
4. **虚引用，PhantomReference**
    用完就消失。

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#int%E3%80%81Integer%E6%9C%89%E4%BB%80%E4%B9%88%E5%8C%BA%E5%88%AB "int、Integer有什么区别")int、Integer有什么区别

* Integer是int的包装类，int则是java的一种基本数据类型

* Integer变量必须实例化后才能使用，而int变量不需要

* Integer实际是对象的引用，当new一个Integer时，实际上是生成一个指针指向此对象，所以两个new出来的Integer永远不相等；而int则是直接存储数据值

* Integer的默认值是null，int的默认值是0

* Integer变量和int变量比较时，只要两个变量的值是向等的，则结果为true（因为包装类Integer和基本数据类型int比较时，java会自动拆包装为int，然后进行比较，实际上就变为两个int变量的比较）

* 非new生成的Integer变量和new Integer()生成的变量比较时，结果为false。（因为非new生成的Integer变量指向的是java常量池中的对象，而new Integer()生成的变量指向堆中新建的对象，两者在内存中的地址不同）

* 对于两个非new生成的Integer对象，进行比较时，如果两个变量的值在区间-128到127之间，则比较结果为true，如果两个变量的值不在此区间，则比较结果为false

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%8A%A8%E6%80%81%E6%9D%83%E9%99%90%E9%80%82%E9%85%8D%E9%97%AE%E9%A2%98%E3%80%81%E6%8D%A2%E8%82%A4%E5%AE%9E%E7%8E%B0%E5%8E%9F%E7%90%86 "动态权限适配问题、换肤实现原理")动态权限适配问题、换肤实现原理

这方面看下鸿洋大神的博文吧
[Android 6.0 运行时权限处理完全解析](https://blog.csdn.net/lmj623565791/article/details/50709663)

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#SharedPreference%E5%8E%9F%E7%90%86%EF%BC%8C%E8%83%BD%E5%90%A6%E8%B7%A8%E8%BF%9B%E7%A8%8B%EF%BC%9F%E5%A6%82%E4%BD%95%E5%AE%9E%E7%8E%B0%EF%BC%9F "SharedPreference原理，能否跨进程？如何实现？")SharedPreference原理，能否跨进程？如何实现？

《Android开发艺术探索》64 page
[Android SharedPreference 支持多进程](https://www.jianshu.com/p/875d13458538)
[Android 7.0 行为变更 通过FileProvider在应用间共享文件吧](https://blog.csdn.net/lmj623565791/article/details/72859156)

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%93%8D%E5%BA%94%E9%80%9F%E5%BA%A6%E4%BC%98%E5%8C%96 "响应速度优化")响应速度优化

Activity如果5秒之内无法响应屏幕触碰事件和键盘输入事件，就会出现ANR；
而BroadcastReceiver如果10秒之内还未执行操作也会出现ANR，
Serve20秒会出现ANR 为了避免ANR，可以开启子线程执行耗时操作，但是子线程不能更新UI，因此需要Handler消息机制、AsyncTask、IntentService进行线程通信。
备：出现ANR时，adb pull data/anr/tarces.txt 结合log分析

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8F "设计模式")设计模式

1. ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%8D%95%E4%BE%8B%E6%A8%A1%E5%BC%8F%EF%BC%9A%E5%A5%BD%E5%87%A0%E7%A7%8D%E5%86%99%E6%B3%95%EF%BC%8C%E8%A6%81%E6%B1%82%E4%BC%9A%E6%89%8B%E5%86%99%EF%BC%8C%E5%88%86%E6%9E%90%E4%BC%98%E5%8A%A3%E3%80%82%E4%B8%80%E8%88%AC%E5%8F%8C%E9%87%8D%E6%A0%A1%E9%AA%8C%E9%94%81%E4%B8%AD%E7%94%A8%E5%88%B0volatile%EF%BC%8C%E9%9C%80%E8%A6%81%E5%88%86%E6%9E%90volatile%E7%9A%84%E5%8E%9F%E7%90%86 "单例模式：好几种写法，要求会手写，分析优劣。一般双重校验锁中用到volatile，需要分析volatile的原理")单例模式：好几种写法，要求会手写，分析优劣。一般双重校验锁中用到volatile，需要分析volatile的原理

2. ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E8%A7%82%E5%AF%9F%E8%80%85%E6%A8%A1%E5%BC%8F%EF%BC%9A%E8%A6%81%E6%B1%82%E4%BC%9A%E6%89%8B%E5%86%99%EF%BC%8C%E6%9C%89%E4%BA%9B%E9%9D%A2%E8%AF%95%E5%AE%98%E4%BC%9A%E9%97%AE%E4%BD%A0%E5%9C%A8%E9%A1%B9%E7%9B%AE%E4%B8%AD%E7%94%A8%E5%88%B0%E4%BA%86%E5%90%97%EF%BC%9F%E5%AE%9E%E5%9C%A8%E6%B2%A1%E6%9C%89%E5%88%B0%E7%9A%84%E5%8F%AF%E4%BB%A5%E8%AE%B2%E4%B8%80%E8%AE%B2EventBus%EF%BC%8C%E5%AE%83%E7%94%A8%E5%88%B0%E7%9A%84%E5%B0%B1%E6%98%AF%E8%A7%82%E5%AF%9F%E8%80%85%E6%A8%A1%E5%BC%8F "观察者模式：要求会手写，有些面试官会问你在项目中用到了吗？实在没有到的可以讲一讲EventBus，它用到的就是观察者模式")观察者模式：要求会手写，有些面试官会问你在项目中用到了吗？实在没有到的可以讲一讲EventBus，它用到的就是观察者模式

3. ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E9%80%82%E9%85%8D%E5%99%A8%E6%A8%A1%E5%BC%8F%EF%BC%9A%E8%A6%81%E6%B1%82%E4%BC%9A%E6%89%8B%E5%86%99%EF%BC%8C%E6%9C%89%E4%BA%9B%E5%85%AC%E5%8F%B8%E4%BC%9A%E9%97%AE%E5%92%8C%E8%A3%85%E9%A5%B0%E5%99%A8%E6%A8%A1%E5%BC%8F%E3%80%81%E4%BB%A3%E7%90%86%E6%A8%A1%E5%BC%8F%E6%9C%89%E4%BB%80%E4%B9%88%E5%8C%BA%E5%88%AB%EF%BC%9F "适配器模式：要求会手写，有些公司会问和装饰器模式、代理模式有什么区别？")适配器模式：要求会手写，有些公司会问和装饰器模式、代理模式有什么区别？

4. ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%BB%BA%E9%80%A0%E8%80%85%E6%A8%A1%E5%BC%8F-%E5%B7%A5%E5%8E%82%E6%A8%A1%E5%BC%8F%EF%BC%9A%E8%A6%81%E6%B1%82%E4%BC%9A%E6%89%8B%E5%86%99 "建造者模式+工厂模式：要求会手写")建造者模式+工厂模式：要求会手写

5. ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E7%AD%96%E7%95%A5%E6%A8%A1%E5%BC%8F%EF%BC%9A%E8%BF%99%E4%B8%AA%E9%97%AE%E5%BE%97%E6%AF%94%E8%BE%83%E5%B0%91%EF%BC%8C%E4%B8%8D%E8%BF%87%E6%9C%89%E4%BA%9B%E5%81%9A%E7%94%B5%E5%95%86%E7%9A%84%E4%BC%9A%E9%97%AE%E3%80%82 "策略模式：这个问得比较少，不过有些做电商的会问。")策略模式：这个问得比较少，不过有些做电商的会问。

6. ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#MVC%E3%80%81MVP%E3%80%81MVVM%EF%BC%9A%E6%AF%94%E8%BE%83%E5%BC%82%E5%90%8C%EF%BC%8C%E9%80%89%E6%8B%A9%E4%B8%80%E7%A7%8D%E4%BD%A0%E6%8B%BF%E6%89%8B%E7%9A%84%E7%9D%80%E9%87%8D%E8%AE%B2%E5%B0%B1%E8%A1%8C "MVC、MVP、MVVM：比较异同，选择一种你拿手的着重讲就行")MVC、MVP、MVVM：比较异同，选择一种你拿手的着重讲就行

    阅读 《Android源码设计模式》最后一章483~892

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84 "数据结构")数据结构

1. ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#HashMap%E3%80%81LinkedHashMap%E3%80%81ConcurrentHashMap%EF%BC%8C%E5%9C%A8%E7%94%A8%E6%B3%95%E5%92%8C%E5%8E%9F%E7%90%86%E4%B8%8A%E6%9C%89%E4%BB%80%E4%B9%88%E5%B7%AE%E5%BC%82%EF%BC%8C "HashMap、LinkedHashMap、ConcurrentHashMap，在用法和原理上有什么差异，")HashMap、LinkedHashMap、ConcurrentHashMap，在用法和原理上有什么差异，

    很多公司会考HashMap原理，通过它做一些扩展，比如中国13亿人口年龄的排序问题，年龄对应桶的个数，年龄相同和hash相同问题类似。
    [Java HashMap工作原理及实现](https://yikun.github.io/2015/04/01/Java-HashMap%E5%B7%A5%E4%BD%9C%E5%8E%9F%E7%90%86%E5%8F%8A%E5%AE%9E%E7%8E%B0/)
    [HashMap的实现原理详解](http://bijian1013.iteye.com/blog/1998847)
2. ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#ArrayList%E5%92%8CLinkedList%E5%AF%B9%E6%AF%94%EF%BC%8C%E8%BF%99%E4%B8%AA%E7%9B%B8%E5%AF%B9%E7%AE%80%E5%8D%95%E4%B8%80%E7%82%B9%E3%80%82 "ArrayList和LinkedList对比，这个相对简单一点。")ArrayList和LinkedList对比，这个相对简单一点。

    [Java关于数据结构的实现：表、栈与队列&ArrayList和LinkedList实现原理](https://juejin.im/post/59cc5559f265da064b47112f)

3. ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%B9%B3%E8%A1%A1%E4%BA%8C%E5%8F%89%E6%A0%91%E3%80%81%E4%BA%8C%E5%8F%89%E6%9F%A5%E6%89%BE%E6%A0%91%E3%80%81%E7%BA%A2%E9%BB%91%E6%A0%91%EF%BC%8C%E8%BF%99%E5%87%A0%E4%B8%AA%E6%88%91%E4%B9%9F%E8%A2%AB%E8%80%83%E5%88%B0%E3%80%82 "平衡二叉树、二叉查找树、红黑树，这几个我也被考到。")平衡二叉树、二叉查找树、红黑树，这几个我也被考到。

    [Java关于数据结构的实现：树](https://juejin.im/post/59cc55b95188250b4007539b)

4. ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Set%E5%8E%9F%E7%90%86%EF%BC%8C%E8%BF%99%E4%B8%AA%E5%92%8CHashMap%E8%80%83%E5%BE%97%E6%9C%89%E7%82%B9%E7%B1%BB%E4%BC%BC%EF%BC%8C%E8%80%83hash%E7%AE%97%E6%B3%95%E7%9B%B8%E5%85%B3%EF%BC%8C%E8%A2%AB%E9%97%AE%E5%88%B0%E8%BF%87%E5%B8%B8%E7%94%A8hash%E7%AE%97%E6%B3%95%E3%80%82HashSet%E5%86%85%E9%83%A8%E7%94%A8%E5%88%B0%E4%BA%86HashMap%E3%80%82 "Set原理，这个和HashMap考得有点类似，考hash算法相关，被问到过常用hash算法。HashSet内部用到了HashMap。")Set原理，这个和HashMap考得有点类似，考hash算法相关，被问到过常用hash算法。HashSet内部用到了HashMap。

5. ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E7%BC%93%E5%AD%98%E6%B7%98%E6%B1%B0%E7%AD%96%E7%95%A5-Lru "缓存淘汰策略(Lru)")缓存淘汰策略(Lru)

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E7%AE%97%E6%B3%95 "算法")算法

1. ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%B8%B8%E7%94%A8%E7%9A%84%E5%87%A0%E7%A7%8D%E7%AE%97%E6%B3%95 "常用的几种算法")常用的几种算法

    [排序算法总结](https://juejin.im/post/59fbe7766fb9a0451c39bf21)

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E7%83%AD%E6%9B%B4%E6%96%B0%E3%80%81%E7%83%AD%E4%BF%AE%E5%A4%8D%E3%80%81%E6%8F%92%E4%BB%B6%E5%8C%96-%E8%BF%99%E4%B8%80%E5%9D%97%E8%A6%81%E6%B1%82%E9%AB%98%E7%82%B9%EF%BC%8C%E4%B8%80%E8%88%AC%E9%AB%98%E7%BA%A7%E5%B7%A5%E7%A8%8B%E5%B8%88%E6%98%AF%E9%9C%80%E8%A6%81%E7%90%86%E8%A7%A3%E7%9A%84 "热更新、热修复、插件化(这一块要求高点，一般高级工程师是需要理解的)")热更新、热修复、插件化(这一块要求高点，一般高级工程师是需要理解的)

[Android 插件化和热修复知识梳理](https://www.jianshu.com/p/704cac3eb13d)
[滴滴插件化方案 VirtualApk 源码解析](https://blog.csdn.net/lmj623565791/article/details/75000580)
[Android 增量更新完全解析 是增量不是热修复](https://blog.csdn.net/lmj623565791/article/details/52761658)

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E6%BA%90%E7%A0%81%E7%90%86%E8%A7%A3 "源码理解")源码理解

项目中多多少少会用到开源框架，很多公司都喜欢问原理和是否看过源码，比如网络框架Okhttp，这是最常用的，现在Retrofit+RxJava也很流行。

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E7%BD%91%E7%BB%9C%E6%A1%86%E6%9E%B6%E5%BA%93-Okhttp "网络框架库 Okhttp")网络框架库 Okhttp

    okhttp源码一定要去看下，里面几个关键的类要记住，还有连接池，拦截器都需要理解。被问到如何给某些特定域名的url增加header，如果是自己封装的代码，可以在封装Request中可以解决，也可以增加拦截器，通过拦截器去做。

    推荐一篇讲解[Okhttp](https://juejin.im/post/5a704ed05188255a8817f4c9)不错的文章
    [Android OkHttp完全解析 是时候来了解OkHttp了](https://blog.csdn.net/lmj623565791/article/details/47911083)

2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E6%B6%88%E6%81%AF%E9%80%9A%E7%9F%A5-EventBus "消息通知 EventBus")消息通知 EventBus

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#EventBus%E5%8E%9F%E7%90%86%EF%BC%9A%E5%BB%BA%E8%AE%AE%E7%9C%8B%E4%B8%8B%E6%BA%90%E7%A0%81%EF%BC%8C%E4%B8%8D%E5%A4%9A%E3%80%82%E5%86%85%E9%83%A8%E5%AE%9E%E7%8E%B0%EF%BC%9A%E8%A7%82%E5%AF%9F%E8%80%85%E6%A8%A1%E5%BC%8F-%E6%B3%A8%E8%A7%A3-%E5%8F%8D%E5%B0%84 "EventBus原理：建议看下源码，不多。内部实现：观察者模式+注解+反射")EventBus原理：建议看下源码，不多。内部实现：观察者模式+注解+反射

    详见文章[EventBus](https://juejin.im/post/5ab9fb4951882521d6579830)

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#EventBus%E5%8F%AF%E5%90%A6%E8%B7%A8%E8%BF%9B%E7%A8%8B%E9%97%AE%E9%A2%98%EF%BC%9F%E4%BB%A3%E6%9B%BFEventBus%E7%9A%84%E6%96%B9%E6%B3%95%EF%BC%88RxBus%EF%BC%89 "EventBus可否跨进程问题？代替EventBus的方法（RxBus）")EventBus可否跨进程问题？代替EventBus的方法（RxBus）

    [RxBus真的能替代EventBus吗？](https://www.jianshu.com/p/669eda5dc5a4)

3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E5%9B%BE%E7%89%87%E5%8A%A0%E8%BD%BD%E5%BA%93%EF%BC%88Fresco%E3%80%81Glide%E3%80%81Picasso%EF%BC%89 "图片加载库（Fresco、Glide、Picasso）")图片加载库（Fresco、Glide、Picasso）

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E9%A1%B9%E7%9B%AE%E4%B8%AD%E9%80%89%E6%8B%A9%E4%BA%86%E5%93%AA%E4%B8%AA%E5%9B%BE%E7%89%87%E5%8A%A0%E8%BD%BD%E5%BA%93%EF%BC%9F%E4%B8%BA%E4%BB%80%E4%B9%88%E9%80%89%E6%8B%A9%E5%AE%83%EF%BC%9F%E5%85%B6%E4%BB%96%E5%BA%93%E4%B8%8D%E5%A5%BD%E5%90%97%EF%BC%9F%E8%BF%99%E5%87%A0%E4%B8%AA%E5%BA%93%E7%9A%84%E5%8C%BA%E5%88%AB "项目中选择了哪个图片加载库？为什么选择它？其他库不好吗？这几个库的区别")项目中选择了哪个图片加载库？为什么选择它？其他库不好吗？这几个库的区别

    [Fresco](https://juejin.im/post/5a7568825188257a7a2d9ddb)
    [Glide](https://www.jianshu.com/p/7ce7b02988a4)
    [Glide 对比 Picasso](http://jcodecraeer.com/a/anzhuokaifa/androidkaifa/2015/0327/2650.html)

    ###### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E9%A1%B9%E7%9B%AE%E4%B8%AD%E9%80%89%E6%8B%A9%E5%9B%BE%E7%89%87%E5%BA%93%E5%AE%83%E7%9A%84%E5%8E%9F%E7%90%86%EF%BC%8C "项目中选择图片库它的原理，")项目中选择图片库它的原理，

    如Glide（LruCache结合弱引用），那么面试官会问LruCache原理，进而问LinkedHashMap原理，这样一层一层地问，所以建议看到不懂的追进去看。如Fresco是用来MVC设计模式，5.0以下是用了共享内存，那共享内存怎么用？Fresco怎么实现圆角？Fresco怎么配置缓存？

    [Android开源框架源码鉴赏：LruCache与DiskLruCache](https://juejin.im/post/5a6da6e7f265da3e303cbcb6)

#### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#%E6%96%B0%E6%8A%80%E6%9C%AF "新技术")新技术

1. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#RxJava%E3%80%81RxBus%E3%80%81RxAndroid%EF%BC%8C%E8%BF%99%E4%B8%AA%E5%9C%A8%E9%9D%A2%E8%AF%95%E6%83%B3%E5%8E%BB%E7%9A%84%E5%85%AC%E5%8F%B8%E6%97%B6%EF%BC%8C%E5%8F%AF%E4%BB%A5%E5%8F%8D%E7%BC%96%E8%AF%91%E4%B8%8B%E4%BB%96%E4%BB%AC%E7%9A%84%E5%8C%85%EF%BC%8C%E7%9C%8B%E4%B8%8B%E6%98%AF%E4%B8%8D%E6%98%AF%E7%94%A8%E5%88%B0%EF%BC%8C%E5%A6%82%E6%9E%9C%E7%94%A8%E5%88%B0%E4%BA%86%EF%BC%8C%E9%9D%A2%E8%AF%95%E8%BF%87%E7%A8%8B%E9%9A%BE%E5%85%8D%E4%BC%9A%E9%97%AE%E9%81%93%EF%BC%8C%E5%A6%82%E6%9E%9C%E6%B2%A1%E6%9C%89%EF%BC%8C%E4%B9%9F%E5%8F%AF%E4%BB%A5%E5%BF%BD%E7%95%A5%EF%BC%8C%E4%BD%86%E5%AD%A6%E4%B9%A0%E5%BF%83%E5%BC%BA%E7%9A%84%E5%90%8C%E5%AD%A6%E5%8F%AF%E4%BB%A5%E7%9C%8B%E4%B8%8B%EF%BC%8C%E6%AF%94%E8%BE%83%E6%98%AF%E6%AF%94%E8%BE%83%E7%81%AB%E7%9A%84%E6%A1%86%E6%9E%B6%E3%80%82 "RxJava、RxBus、RxAndroid，这个在面试想去的公司时，可以反编译下他们的包，看下是不是用到，如果用到了，面试过程难免会问道，如果没有，也可以忽略，但学习心强的同学可以看下，比较是比较火的框架。")RxJava、RxBus、RxAndroid，这个在面试想去的公司时，可以反编译下他们的包，看下是不是用到，如果用到了，面试过程难免会问道，如果没有，也可以忽略，但学习心强的同学可以看下，比较是比较火的框架。

2. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Retrofit%EF%BC%8C%E7%86%9F%E7%BB%83okhttp%E7%9A%84%E5%90%8C%E5%AD%A6%E5%BB%BA%E8%AE%AE%E7%9C%8B%E4%B8%8B%EF%BC%8C%E5%90%AC%E8%AF%B4%E7%BB%93%E5%90%88RxJava%E5%BE%88%E7%88%BD%E3%80%82 "Retrofit，熟练okhttp的同学建议看下，听说结合RxJava很爽。")Retrofit，熟练okhttp的同学建议看下，听说结合RxJava很爽。

3. ##### [](http://www.yyglike.com/2018/03/31/Android%E5%BC%80%E5%8F%91%E8%89%BA%E6%9C%AF%E6%8E%A2%E7%B4%A2%E6%80%BB%E7%BB%93/#Kotlin "Kotlin")Kotlin

#### LeakCanay 原理

LeakCanay的入口是在application的onCreate()方法中声明的，其实用的就是Application的ActivityLifecycleCallbacks回调接口监听所有activity的onDestory()的，在这个方法进行RefWatcher.watch对这个对象进行监控。
具体是这样做的，封装成带key的弱引用对象，然后GC看弱引用对象有没有回收，没有回收的话就怀疑是泄漏了，需要二次确认。然后生成HPROF文件，分析这个快照文件有没有存在带这个key值的泄漏对象，如果没有，那么没有泄漏，否则找出最短路径，打印给我们，我们就能够找到这个泄漏对象了。

