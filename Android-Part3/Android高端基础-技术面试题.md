# Android面试题

Android面试题除了Android基础之外，更多的问的是一些源码级别的、原理这些等。所以想去大公司面试，一定要多看看源码和实现方式，常用框架可以试试自己能不能手写实现一下，锻炼一下自己。

### 一、Android基础知识点

#####  四大组件的生命周期和简单用法
![](http://7xrk8u.com1.z0.glb.clouddn.com/15241125347971.png)



#####  Activity之间的通信方式
Intent、Broadcast或者LocalBroadcast、使用静态变量(不推荐)、用数据存储的方式ContentProvider
#####  Activity各种情况下的生命周期

```
--onCreate() (Activity创建时调用 )
--onStart()(可见未获取焦点，无法与之交互 )
--onResume()(可见已获取焦点，可与之交互 )
--onPause()(可见，失去焦点 )
--onStop()(不可见 )
--onRestart()(Activity重启)
--onDestroy()(Activity销毁)

--onSaceInstanceState可能会被回收的时候调用,与上面的先后顺序各个Android版本不同
--onResotreInstanceState没有被回收的话就不会调用
--onConfigurationChanged

1.正常通过startActivity或者桌面快捷方式直接启动直到可见可正常交互
onCreate()--onStart()--onResume()
2.正常销毁,(在onResume之后),执行finish()方法或者点击回退按钮
onPause()--onStop()--onDestroy()
3.在onCreate()里调用finish()
onCreate--onDestroy()
4.在onStart()里调用finish()
onCreate--onStart--onStop--onDestroy
5.正常启动后按下Home键(API4.4onSaveInstanceState在onPause之后)
onPause--onSaveInstanceState--onStop
6.按下Home键没回收之前再点开
onRestart--onStart--onResume
7.出现Dialog
不调用任何生命周期
8.出现Dialog主题的Activity
onPause--onSaveInstanceState
9.去掉dialog
onResume
10.在manifest文件设置android:configChanges属性orientation|keyboard|screenSize
切换横/竖屏
onConfigurationChanged
11..在manifest文件设置android:configChanges属性orientation|keyboard
在4.4以上横屏
onPause--onSaveInstanceState--onStop--onCreate()--onStart()--onResume()
4.4以下(不一定,跟是否改变了屏幕大小有关)横屏
onConfigurationChanged
竖屏4.4以下
onConfigurationChanged
竖屏4.4以上
onPause--onSaveInstanceState--onStop--onCreate()--onStart()--onResume()
11..在manifest文件设置android:configChanges属性orientation
切横屏
onPause--onSaveInstanceState--onStop--onCreate()--onStart()--onResume()
切竖屏4.4以上一次2.3以下2次
onPause--onSaveInstanceState--onStop--onCreate()--onStart()--onResume()
onPause--onSaveInstanceState--onStop--onCreate()--onStart()--onResume()
12.设置launchMode的情况下,如果只是把Acitvity调回到前台
会执行onNewIntent()

```
#####  横竖屏切换的时候，Activity 各种情况下的生命周期

* 1、不设置Activity的android:configChanges时，切屏会重新调用各个生命周期，切横屏时会执行一次，切竖屏时会执行两次
* 2、设置Activity的android:configChanges="orientation"时，切屏还是会重新调用各个生命周期，切横、竖屏时只会执行一次
* 3、设置Activity的android:configChanges="orientationkeyboardHidden"时，切屏不会重新调用各个生命周期，只会执行onConfigurationChanged方法

#####  两个Activity 之间跳转时必然会执行的是哪几个方法？

一般情况下比如说有两个activity,分别叫A,B,当在A里面激活B组件的时候, A会调用 onPause() 方法,然后 B 调用 onCreate() ,onStart(), onResume()。 这个时候 B 覆盖了窗体, A 会调用 onStop()方法. 如果 B 是个透明的,或者是对话框的样式, 就 不会调用 A 的 onStop()方法。

#####  前台切换到后台，然后再回到前台，Activity生命周期回调方法。弹出Dialog，生命值周期回调方法。

#####  Activity状态保存于恢复
Activity 由于异常终止时，系统会调用 onSaveInstanceState()来保存 Activity 状态(onStop()之前和onPause()没有既定的时序关系)。当重建时，会调用 onRestoreInstanceState()，并且把 Activity 销毁时 onSaveInstanceState()方法所保存的 Bundle 对象参数同时传递给 onSaveInstanceState()和onCreate()方法。因此，可通过 onRestoreInstanceState()方法来恢复 Activity 的状态，该方法的调用时机是在 onStart()之后。
onCreate()和 onRestoreInstanceState()的区别：onRestoreInstanceState()回调则表明其中Bundle对象非空，不用加非空判断。onCreate()需要非空判断。建议使用onRestoreInstanceState().
#####  fragment各种情况下的生命周期
![](http://7xrk8u.com1.z0.glb.clouddn.com/15241136133141.jpg)

#####  Fragment状态保存、startActivityForResult是哪个类的方法，在什么情况下使用？
使用Fragment的startActivity（）方法时，FragmentActivity的onActivityResult（）方法会回调相应的Fragment的onActivityResult（）方法，所以在重写FragmentActivity的onActivityResult（）方法时，注意调super.onActivityResult（）。
#####  如何实现Fragment的滑动？

这个不就是ViewPager + Fragment的实现么

#####  **Activity 怎么和Service 绑定？**
Activity 通过 bindService(Intent service, ServiceConnection conn, int flags) 跟服务 进行绑定, 当绑定成功的时候服务会将代理对象通过 onBind() 方法传给 conn, 这样我们就拿到了服务提供的服务代理对象.


#####  怎么在Activity 中启动自己对应的Service？

1. Activity通过bindService(Intent service, ServiceConnection  conn, int flags)跟Service进行绑定。
2. 绑定成功以后，Service会将代理对象通过回调的形式传递给MyServiceConnection，这样我们就获取Service提供的代理对象


#####  service和activity怎么进行数据交互？
通过Binder对象、通过broadcast(广播)的形式

#####  请描述一下Service 的生命周期
![](http://7xrk8u.com1.z0.glb.clouddn.com/15241135570677.png)
startService/stopService
onCreate()->onStartCommand()->onDestroy()
第一次调用会触发 onCreate()和 onStartCommand，之后每次启动服务只触发 onStartCommand()
无论 startService 多少次，stopService 一次就会停止服务
bindService/unbindService
onCreate->onBind()->onUnbind()->onDestroy()
第一次 bindService 会触发 onCreate 和 onBind，以后每次 bindService 都不会触发任何回调
bindService 启动的生命周期依附于启动它的 Context

#####  说说ContentProvider、ContentResolver、ContentObserver 之间的关系

一个应用实现ContentProvider来提供内容给别的应用来操作， 通过ContentResolver来操作别的应用数据，当然在自己的应用中也可以。 
ContentObserver——内容观察者，目的是观察(捕捉)特定Uri引起的数据库的变化，继而做一些相应的处理，它类似于数据库技术中的触发器(Trigger)，当ContentObserver所观察的Uri发生变化时，便会触发它。触发器分为表触发器、行触发器，相应地ContentObserver也分为“表“ContentObserver、“行”ContentObserver，当然这是与它所监听的Uri MIME Type有关的。

#####   本地广播和全局广播有什么差别？

`BroadcastReceiver`是针对应用间、应用与系统间、应用内部进行通信的一种方式
`LocalBroadcastReceiver`仅在自己的应用内发送接收广播，也就是只有自己的应用能收到，数据更加安全广播只在这个程序里，而且效率更高。
`LocalBroadcastReceiver`不能静态注册，只能采用动态注册的方式。
在发送和注册的时候采用，`LocalBroadcastManager`的`sendBroadcast`方法和`registerReceiver`方法


#####  AlertDialog,popupWindow,Activity区别

（1）Popupwindow在显示之前一定要设置宽高，Dialog无此限制。
（2）Popupwindow默认不会响应物理键盘的back，除非显示设置了popup.setFocusable(true);而在点击back的时候，Dialog会消失。
（3）Popupwindow不会给页面其他的部分添加蒙层，而Dialog会。
（4）Popupwindow没有标题，Dialog默认有标题，可以通过dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);取消标题
（5）二者显示的时候都要设置Gravity。如果不设置，Dialog默认是Gravity.CENTER。
（6）二者都有默认的背景，都可以通过setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));去掉。
其中最本质的差别就是：AlertDialog是非阻塞式对话框：AlertDialog弹出时，后台还可以做事情；而PopupWindow是阻塞式对话框：PopupWindow弹出时，程序会等待，在PopupWindow退出前，程序一直等待，只有当我们调用了dismiss方法的后，PopupWindow退出，程序才会向下执行。这两种区别的表现是：AlertDialog弹出时，背景是黑色的，但是当我们点击背景，AlertDialog会消失，证明程序不仅响应AlertDialog的操作，还响应其他操作，其他程序没有被阻塞，这说明了AlertDialog是非阻塞式对话框；PopupWindow弹出时，背景没有什么变化，但是当我们点击背景的时候，程序没有响应，只允许我们操作PopupWindow，其他操作被阻塞。我们在写程序的过程中可以根据自己的需要选择使用Popupwindow或者是Dialog。

由于开始不知道AlertDialog是非阻塞式对话框，以为是在用户在点击了弹框上面的操作后才会执行后面的代码，后来才知道我错了 

#####  Application 和 Activity 的 Context 对象的区别
![](http://7xrk8u.com1.z0.glb.clouddn.com/15241133178379.jpg)

#####  LinearLayout、RelativeLayout、FrameLayout的特性及对比，并介绍使用场景。
FrameLayout: Android 中最简单布局，所有控件默认出现在左上角，可以使用 layout_margin，layout_gravity 等属性控制子控件的相对布局位置。
LinearLayout: 一行或者一列布局空间，可使用 orientation=horizontal|vertical 来控制布局方式。
AbsoluteLayout: 自定义空间的x，y 来调整位置。
RelativeLayout: 相对布局，通过相关属性来定义空间的位置
android:layout_centerInParent=”true|false”
android:layout_centerHorizontal=”true|false”
android:layout_alignParentRight=”true|false”

TableLayout: 将字元素的位置以表格形式进行布局。

#####  介绍下SurfaceView
View和SurfaceView的区别:
1 . View适用于主动更新的情况，而SurfaceView则适用于被动更新的情况，比如频繁刷新界面。
2 . View在主线程中对页面进行刷新，而SurfaceView则开启一个子线程来对页面进行刷新。
3 . View在绘图时没有实现双缓冲机制，SurfaceView在底层机制中就实现了双缓冲机制。

#####  RecycleView的使用

#####  序列化的作用，以及Android两种序列化的区别
1）在使用内存的时候，Parcelable比Serializable性能高，所以推荐使用Parcelable。
2）Serializable在序列化的时候会产生大量的临时变量，从而引起频繁的GC。
3）Parcelable不能使用在要将数据存储在磁盘上的情况，因为Parcelable不能很好的保证数据的持续性在外界有变化的情况下。尽管Serializable效率低点，但此时还是建议使用Serializable 。
#####  插值器、 估值器
* 插值器Interpolator：
根据事件流逝的百分比 计算 当前属性改变的百分比.举个例子,就是根据你设置的Durantion流逝的百分比,来改变位移(tanslate)的速度.
场景：实现非线性运动的动画效果
非线性运动：动画改变的速率不是一成不变的，如加速 & 减速运动都属于非线性运动
目前,Android中已经有了几种插值器,如下:
AccelerateDecelerateInterpolator 在动画开始与结束的地方速率改变比较慢，在中间的时候加速
AccelerateInterpolator 在动画开始的地方速率改变比较慢，然后开始加速
AnticipateInterpolator 开始的时候向后甩一点然后向前
AnticipateOvershootInterpolator 开始的时候向后甩一点然后向前超过设定值一点然后返回
BounceInterpolator 动画结束的时候弹起，类似皮球落地
CycleInterpolator 动画循环播放特定的次数回到原点，速率改变沿着正弦曲线
DecelerateInterpolator 在动画开始的地方快然后慢
LinearInterpolator 以常量速率改变
OvershootInterpolator 向前超过设定值一点然后返回

* 估值器Evaluator
设置 属性值 从初始值过渡到结束值 的变化具体数值
插值器（Interpolator）决定 值 的变化规律（匀速、加速blabla），即决定的是变化趋势；
而具体变化数值则交给估值器evaluator.

fraction：动画完成度,插值器getInterpolation（）的返回值(input的值决定了fraction的值),代表时间流逝的百分比
startValue：动画的初始值、endValue：动画的结束值

``` java
public interface TypeEvaluator<T> {

    /**
     * This function returns the result of linearly interpolating the start and end values, with
     * <code>fraction</code> representing the proportion between the start and end values. The
     * calculation is a simple parametric calculation: <code>result = x0 + t * (x1 - x0)</code>,
     * where <code>x0</code> is <code>startValue</code>, <code>x1</code> is <code>endValue</code>,
     * and <code>t</code> is <code>fraction</code>.
     *
     * @param fraction   The fraction from the starting to the ending values
     * @param startValue The start value.
     * @param endValue   The end value.
     * @return A linear interpolation between the start and end values, given the
     *         <code>fraction</code> parameter.
     */
    public T evaluate(float fraction, T startValue, T endValue);

}
```
常见的实现类：
IntEvaluator Int类型估值器，返回int类型的属性改变
FloatEvaluator Float类型估值器，返回Float类型属性改变
ArgbEvaluator 颜色类型估值器
插值器的input值 和 估值器fraction关系
input的值决定了fraction的值：input值经过计算后传入到插值器的getInterpolation（），然后通过实现getInterpolation（）中的逻辑算法，根据input值来计算出一个返回值，而这个返回值就是fraction了} }


#####  Android中数据存储方式

* SQLite 数据库存储
* 文件存储
* SharedPreference 存储
* 网络存储

### 二、Android源码相关分析

#####  Android动画框架实现原理

Android 动画就是通过 ParentView 来不断调整 ChildView 的画布坐标系来实现的


#####  Android各个版本API的区别


##### Requestlayout，onlayout，onDraw，DrawChild区别与联系

requestLayout()方法 ：会导致调用measure()过程 和 layout()过程 。 将会根据标志位判断是否需要ondraw

onLayout()方法(如果该View是ViewGroup对象，需要实现该方法，对每个子视图进行布局)

调用onDraw()方法绘制视图本身   (每个View都需要重载该方法，ViewGroup不需要实现该方法)

drawChild()去重新回调每个子视图的draw()方法


#####  invalidate和postInvalidate的区别及使用

Android提供了Invalidate方法实现界面刷新，但是Invalidate不能直接在线程中调用，因为他是违背了单线程模型：Android UI操作并不是线程安全的，并且这些操作必须在UI线程中调用。 
invalidate() 是用来刷新View的，必须是在UI线程中进行工作。比如在修改某个view的显示时，调用invalidate()才能看到重新绘制的界面。 invalidate()的调用是把之前的旧的view从主UI线程队列中pop掉。 一个Android 程序默认情况下也只有一个进程，但一个进程下却可以有许多个线程。

在这么多线程当中，把主要是负责控制UI界面的显示、更新和控件交互的线程称为UI线程，由于onCreate()方法是由UI线程执行的，所以也可以把UI线程理解为主线程。其余的线程可以理解为工作者线程。

invalidate()得在UI线程中被调动，在工作者线程中可以通过Handler来通知UI线程进行界面更新。

而postInvalidate()在工作者线程中被调用

#####  Activity-Window-View三者的差别

Activity像一个工匠（控制单元），Window像窗户（承载模型），View像窗花（显示视图） LayoutInflater像剪刀，Xml配置像窗花图纸。

1. 在Activity中调用attach，创建了一个Window
2. 创建的window是其子类PhoneWindow，在attach中创建PhoneWindow
3. 在Activity中调用setContentView(R.layout.xxx)
4. 其中实际上是调用的getWindow().setContentView()
5. 调用PhoneWindow中的setContentView方法
6. 创建ParentView：作为ViewGroup的子类，实际是创建的DecorView(作为FramLayout的子类）
7. 将指定的R.layout.xxx进行填充，通过布局填充器进行填充【其中的parent指的就是DecorView】
8. 调用到ViewGroup
9. 调用ViewGroup的removeAllView()，先将所有的view移除掉
10. 添加新的view：addView()

Fragment 特点

* Fragment可以作为Activity界面的一部分组成出现；
* 可以在一个Activity中同时出现多个Fragment，并且一个Fragment也可以在多个Activity中使用；
* 在Activity运行过程中，可以添加、移除或者替换Fragment；
* Fragment可以响应自己的输入事件，并且有自己的生命周期，它们的生命周期会受宿主Activity的生命周期影响。

##### 如何优化自定义View
 渲染帧率、内存
 为了加速你的view，对于频繁调用的方法，需要尽量减少不必要的代码。先从onDraw开始，需要特别注意不应该在这里做内存分配的事情，因为它会导致GC，从而导致卡顿。在初始化或者动画间隙期间做分配内存的动作。不要在动画正在执行的时候做内存分配的事情。

你还需要尽可能的减少onDraw被调用的次数，大多数时候导致onDraw都是因为调用了invalidate().因此请尽量减少调用invaildate()的次数。如果可能的话，尽量调用含有4个参数的invalidate()方法而不是没有参数的invalidate()。没有参数的invalidate会强制重绘整个view。

另外一个非常耗时的操作是请求layout。任何时候执行requestLayout()，会使得Android UI系统去遍历整个View的层级来计算出每一个view的大小。如果找到有冲突的值，它会需要重新计算好几次。另外需要尽量保持View的层级是扁平化的，这样对提高效率很有帮助。

如果你有一个复杂的UI，你应该考虑写一个自定义的ViewGroup来执行他的layout操作。与内置的view不同，自定义的view可以使得程序仅仅测量这一部分，这避免了遍历整个view的层级结构来计算大小。这个PieChart 例子展示了如何继承ViewGroup作为自定义view的一部分。PieChart 有子views，但是它从来不测量它们。而是根据他自身的layout法则，直接设置它们的大小。


##### 低版本SDK如何实现高版本api？

自己实现或@TargetApi annotation
##### 描述一次网络请求的流程

通过URL找IP、对IP结果建立TCP连接、一次完整的HTTP请求过程从TCP三次握手建立连接成功后开始，客户端按照指定的格式开始向服务端发送HTTP请求，服务端接收请求后，解析HTTP请求，处理完业务逻辑，最后返回一个HTTP的响应给客户端，HTTP的响应内容同样有标准的格式。无论是什么客户端或者是什么服务端，大家只要按照HTTP的协议标准来实现的话，那么它一定是通用的


##### HttpUrlConnection 和 okhttp关系

* HttpURLConnection
    HttpURLConnection是一种多用途、轻量极的HTTP客户端，使用它来进行HTTP操作可以适用于大多数的应用程序。虽然HttpURLConnection的API提供的比较简单，但是同时这也使得我们可以更加容易地去使用和扩展它。从Android4.4开始HttpURLConnection的底层实现采用的是okHttp。

    * HttpClient
    Apache HttpClient早就不推荐httpclient，5.0之后干脆废弃，后续会删除。6.0删除了HttpClient。

    * OkHttp
    okhttp是高性能的http库，支持同步、异步，而且实现了spdy、http2、websocket协议，api很简洁易用，和volley一样实现了http协议的缓存。picasso就是利用okhttp的缓存机制实现其文件缓存，实现的很优雅，很正确，反例就是UIL（universal image loader），自己做的文件缓存，而且不遵守http缓存机制。

    * volley
    volley是一个简单的异步http库，仅此而已。缺点是不支持同步，这点会限制开发模式。自带缓存，支持自定义请求。不适合大文件上传和下载。
    Volley在Android 2.3及以上版本，使用的是HttpURLConnection，而在Android 2.2及以下版本，使用的是HttpClient。
    Volley自己的定位是轻量级网络交互，适合大量的，小数据传输。
    不过再怎么封装Volley在功能拓展性上始终无法与OkHttp相比。Volley停止了更新，而OkHttp得到了官方的认可，并在不断优化。

    * android-async-http。
    与volley一样是异步网络库，但volley是封装的httpUrlConnection，它是封装的httpClient，而android平台不推荐用HttpClient了，所以这个库已经不适合android平台了。

#####  ActivityThread，AMS，WMS的工作原理

Activity与WIndow：

Activity只负责生命周期和事件处理
Window只控制视图
一个Activity包含一个Window，如果Activity没有Window，那就相当于Service
AMS与WMS：

AMS统一调度所有应用程序的Activity
WMS控制所有Window的显示与隐藏以及要显示的位置

#####  自定义View如何考虑机型适配
获取屏幕的长和宽，进行相应的 PX DP 转换，margin padding 这些值按照比例去生成。
#####  SpareArray原理

使用int[]数组存放key，避免了HashMap中基本数据类型需要装箱的步骤，其次不使用额外的结构体（Entry)，单个元素的存储成本下降。

存放key的数组是有序的（二分查找的前提条件）
如果冲突，新值直接覆盖原值，并且不会返回原值（HashMap会返回原值）
如果当前要插入的 key 的索引上的值为DELETE，直接覆盖
前几步都失败了，检查是否需要gc()并且在该索引上插入数据

#####  IntentService原理及作用是什么？

启动一个单独的线程（工作线程）来处理任务和请求，所有的任务都在该改线程中处理。

因为是在单独的线程中处理任务和请求，其onHandleIntent方法运行在单独的线程中，而非主线程，因此可以执行异步操作。(面试中经常被问到)

按照发送顺序处理任务和请求。当没有任务和请求时，IntentService会自动销毁。因此它不会一直占用资源和内存。

onBind方法的默认实现返回值为null，因此不要尝试调用bindService去调用IntentService。(设计的目的是为了处理简单的异步任务)

IntentService是通过HandlerThread+Handler的方式实现的一个异步操作，可以执行耗时操作同时不会产生ANR的情况，而且具备生命周期管理，是我们执行简单异步任务的一个不错的选择。

#####  进程和 Application 的生命周期
Application是单例模式的类，android系统为每个应用程序创建一个Application类的对象且只创建一个。
启动Application时，系统会创建一个PID，即进程ID，所有的Activity都会在此进程上运行。
Application全局的单例的，所以在不同的Activity,Service中获得的对象都是同一个对象
![](http://7xrk8u.com1.z0.glb.clouddn.com/15241181589820.jpg)

#####  AndroidManifest的作用与理解
Android项目构成之AndroidManifest.xml文件简介 
这个文件的作用是用来声明应用程序的基本的信息的包括：

它使注册在哪一个包下的。
应用程序的版本，包括版本号和版本名
它运行的目标Android SDK和要求
运行它要使用的权限
应用程序和它的所有主要组成部分：活动、服务、提供器、接收器

### 三、常见的一些原理性问题

#####  Handler机制和底层实现

Android消息机制包含: MessageQuene、Handler、Looper、Message.

* Message: 需要春娣的消息，可以传递数据。
* MessageQuene: 消息队列，通过单链表数据结构来维护消息列表，（投递和删除消息）。
* Handler: 消息辅助类，向消息池发送各种消息事件

    > Handler.sendMessage 发送消息
    > Handler.handleMessage 处理消息

* Looper: 不断从消息队里中读取消息，按分发机制将消息分发给目标处理者。

    #### [](http://zhaoxueli.win/2017/09/14/Android%E9%9D%A2%E8%AF%95%E6%80%BB%E7%BB%93/#MessageQuene%E3%80%81Handler-%E5%92%8C-looper-%E4%B8%89%E8%80%85%E4%B9%8B%E9%97%B4%E7%9A%84%E5%85%B3%E7%B3%BB%EF%BC%9A "MessageQuene、Handler 和 looper 三者之间的关系：")MessageQuene、Handler 和 looper 三者之间的关系：

    每个线程中只能存在一个Looper，保存在ThreadLocal中。主线程（UI线程）已经创建了一个Looper，所以在主线程中不需要再创建Looper，其他线程中需要创建Looper。每个线程中可以有多个Handler，即一个 Looper 可以处理来自多个 Handler 的消息。 Looper 中维护一个 MessageQueue，来维护消息队列，消息队列中的 Message 可以来自不同的 Handler。

当调用 handler.sendMessage()发送 message 时，实际上发送到与当前线程绑定的 MessageQuene 中，然后当前线程绑定的 Looper 不断从 MessageQueue 取出新的 Message，调用 msg.target.disspatchMessage(msg) 方法将消息分发到与 Message 绑定的 handler.handleMessage()中。

#####  ThreadLocal原理，实现及如何保证Local属性？
这个类提供线程局部变量。这种在多线程环境下访问（通过get或set方法）时，能保证各个线程里的变量相对独立于其他线程内的变量。ThreadLocal实例通常是private static类型的，用于管理线程上下文。

也就是说，Threadlocal提供了作用范围为线程的局部变量，这种变量只在线程生命周期内起作用。减少了线程内多个方法之间公共变量传递的复杂度。

这里关于线程安全的类有一个普遍适用的原则：如果一个类没有实例私有属性，或者实例私有属性也是无状态的类，那么这个类就是无状态的类。而一个无状态的类肯定是线程安全的类。

ThreadLocal的实现原理：
1.首先获取当前线程
2.获取当前线程中的一个类型为ThreadLocalMap（这个类后面会讲到）的成员变量：threadLocals
3.如果threadLocalMap不为null，这通过当前ThreadLocal的引用作为key获取对应的value e。同时如果e不为null，返回e.value
4.如果threadLocalMap为null或者e为null，通过``setInitialValue``方法返回初始值。并且使用当前ThreadLocal的引用和value作为初始key与value创建一个新的threadLocalMap
总体设计思路：Thread维护了一个Map，key为ThreadLocal实例本身，value为真正需要存储的Object。

这样设计的好处：Map的Entry数量变小，性能提升。并且会随Thread一起销毁。

#####  事件分发中的onTouch 和onTouchEvent 有什么区别，又该如何使用？
从源码中可以看出，这两个方法都是在View的dispatchTouchEvent中调用的，onTouch优先于onTouchEvent执行。如果在onTouch方法中通过返回true将事件消费掉，onTouchEvent将不会再执行。
#####  View刷新机制
父View负责刷新、布局显示子View；而当子View需要刷新时，则是通知父View来完成
#####  View绘制流程
`View`的绘制流程是从`ViewRootImpl`的`performTraversals`方法开始，它经过`measure`、`layout`和`draw`三个过程才能最终将一个`View`绘制出来。
#####  自定义控件原理
1\. 继承ViewGroup 
      例如：ViewGroup、LinearLayout、FrameLayout、RelativeLayout等。
2\. 继承View
      例如：View、TextView、ImageView、Button等。

### 自定义控件基本绘制原理：
View的绘制基本上由measure()、layout()、draw()这个三个函数完成
onMeasure()方法：单一 View，一般重写此方法，针对 wrap_content 情况，规定 View 默认的大小值，避免于 match_parent 情况一致。ViewGroup，若不重写，就会执行和单子View 中相同逻辑，不会测量子 View。一般会重写 onMeasure()方法，循环测量子View。
onLayout()方法:单一 View，不需要实现该方法。ViewGroup 必须实现，该方法是个抽象方法，实现该方法，来对子 View 进行布局。
onDraw()方法：无论单一View，或者ViewGroup都需要实现该方法，因其是个空方法.
#####  AsyncTask原理及不足
AsyncTask可能存在新开大量线程消耗系统资源和导致应用FC的风险，因此，我们需要根据自己的需求自定义不同的线程池。

* **AsyncTask理解**：
1. AsyncTask是Handler与线程池的封装。
2. 网络请求等耗时操作在线程池中完成，通过handler发送给主线程完成UI更新。
3. 使用线程池的主要原因是避免不必要的创建及销毁线程的开销。

* **AsyncTask的不足**：
1. 内存泄漏问题
2. AsyncTask对象必须在主线程中创建，这与内部sHandler有关，下面会解释。
3. AsyncTask对象的execute方法必须在主线程中调用。
4. 一个AsyncTask对象只能调用一次execute方法

#####  为什么不能在子线程更新UI？
ViewRootImpl是在onResume 方法中创建的,根据前面的代码,只有当ViewRootImpl创建时候才会有checkThread这个检查操作,也就是说 只要在onResume之前或者换句话说在ViewRootImpl创建之前,在子线程中更新UI就不会引发异常.
#####  ANR产生的原因是什么？
只有当应用程序的UI线程响应超时才会引起ANR，超时产生原因一般有两种。

当前的事件没有机会得到处理，例如UI线程正在响应另一个事件，当前事件由于某种原因被阻塞了。
当前的事件正在处理，但是由于耗时太长没能及时完成。
根据ANR产生的原因不同，超时事件也不尽相同，从本质上将，产生ANR的原因有三种，大致可以对应到android中四大组件中的三个（Activity/View，BroadcastReceiver和Service）。

* KeyDispatchTimeout
最常见的一种类型，原因就是View的点击事件或者触摸事件在特定的时间（5s）内无法得到响应。
* BroadcastTimeout
原因是BroadcastReceiver的onReceive()函数运行在主线程中，在特定的时间（10s）内无法完成处理。
* ServiceTimeout
比较少出现的一种类型，原因是Service的各个生命周期函数在特定时间（20s）内无法完成处理。
**典型的ANR问题场景**
应用程序UI线程存在耗时操作。例如在UI线程中进行联网请求，数据库操作或者文件操作等。
应用程序的UI线程等待子线程释放某个锁，从而无法处理用户的输入。
耗时的动画需要大量的计算工作，可能导致CPU负载过重。

#####  ANR定位和修正
当发生ANR时，可以通过结合Logcat日志和生成的位于手机内部存储的/data/anr/traces.tex文件进行分析和定位。发生ANR的进程、发生ANR的原因、各进程的CPU使用率、CPU使用汇总
#####  ContentProvider的权限管理(解答：读写分离，权限控制-精确到表级，通过URL控制)
解答：读写分离，权限控制-精确到表级，通过URL控制
#####  计算一个view的嵌套层级
递归遍历
 
```  
 int i = 0;
    private void getParents(ViewParent view){
       if (view.getParent() == null) {
            Log.v("tag", "最终==="+i);
            return;
        }
        i++;
        ViewParent parent = view.getParent();
        Log.v("tag", "i===="+i);
        Log.v("tag", "parent===="+parent.toString());
        getParents(parent);
    }
```
##### 封装View的时候怎么知道view的大小 
https://blog.csdn.net/fwt336/article/details/52979876 
#####  Android线程有没有上限？线程池有没有上限，多少合适？
JVM中可以生成的最大数量由JVM的堆内存大小、Thread的Stack内存大小、系统最大可创建的线程数量
如果是CPU密集型应用，则线程池大小设置为N+1 
如果是IO密集型应用，则线程池大小设置为2N+1

### 四、开发中常见的一些问题

#####  知道哪些混合开发的方式？说出它们的优缺点和各自使用场景？（解答：比如:RN，weex，H5，小程序，WPA等。做Android的了解一些前端js等还是很有好处的)；
解答：比如:RN，weex，H5，小程序，WPA等。做Android的了解一些前端js等还是很有好处的
#####  屏幕适配的处理技巧都有哪些?
方式之-dp
res文件夹下的目录分类、方式之-dimens尺寸、方式之-layout、方式之-代码适配、方式之-weight权重、多个相对的标准权重、单个相对父布局的权重、自动权重
#####  服务器只提供数据接收接口，在多线程或多进程条件下，如何保证数据的有序到达？
#####  动态布局的理解
动态添加java布局、动态添加xml布局
#####  怎么去除重复代码？
能引用就引用
#####  动态权限适配方案，权限组的概念
权限组：

* normal permissions：涵盖应用需要访问数据或资源，但对用户隐私或其他应用操作风险很小的区域。例如，设置时区的权限就是正常权限。如果应用声明其需要正常权限，系统会自动向应用授予该权限。

* dangerous permissions：涵盖应用需要涉及用户隐私信息的数据或资源，或者可能对用户存储的数据或其他应用的操作产生影响的区域。例如，能够读取用户的联系人属于危险权限。如果应用声明其需要危险权限，则用户必须明确向应用授予该权限。
#####  下拉状态栏是不是影响activity的生命周期
Android下拉通知栏不会影响Activity的生命周期方法
#####  如果在onStop的时候做了网络请求，onResume的时候怎么恢复？
#####  Bitmap 使用时候注意什么？

1.要选择合适的图片规格（bitmap类型），
2.降低采样率。BitmapFactory.Options 参数inSampleSize的使用，先把options.inJustDecodeBounds设为true，只是去读取图片的大小
3.复用内存
4.及时回收、压缩图片
5. 尽量不要使用setImageBitmap或setImageResource或BitmapFactory.decodeResource来设置一张大图，因为这些函数在完成decode后，最终都是通过java层的createBitmap来完成的，需要消耗更多内存，可以通过BitmapFactory.decodeStream方法，创建出一个bitmap，再将其设为ImageView的 source。
>1、BitmapConfig的配置
2、使用decodeFile、decodeResource、decodeStream进行解析Bitmap时，配置inDensity和inTargetDensity，两者应该相等,值可以等于屏幕像素密度*0.75f
3、使用inJustDecodeBounds预判断Bitmap的大小及使用inSampleSize进行压缩
4、对Density>240的设备进行Bitmap的适配（缩放Density）
5、2.3版本inNativeAlloc的使用
6、4.4以下版本inPurgeable、inInputShareable的使用
7、Bitmap的回收

#####  Bitmap的recycler()
如果你确信没有进一步的操作就可以调用其recycler方法，释放内存；如果后续有getPixels()与setPixels()的调用就不要随意释放内存，等待GC的回收，否则会抛异常。
#####  ViewPager使用细节，如何设置成每次只初始化当前的Fragment，其他的不初始化？

```
/**
     * 在这里实现Fragment数据的缓加载.
     *
     * @param isVisibleToUser
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            onVisible();
        } else {
            isVisible = false;
            onInvisible();
        }
    }
    protected void onVisible() {
        lazyLoad();
    }

```
#####  点击事件被拦截，但是想传到下面的View，如何操作？

```
 getParent().requestDisallowInterceptTouchEvent(true);
```
#####  CAS介绍（这是阿里巴巴的面试题，我不是很了解，可以参考博客: [CAS简介](http://blog.csdn.net/jly4758/article/details/46673835)）
作用：CAS有3个操作数，内存值V，旧的预期值A，要修改的新值B。当且仅当预期值A和内存值V相同时，将内存值V修改为B，否则什么都不做。

原理：CAS通过调用JNI的代码实现的。JNI:Java Native Interface为JAVA本地调用，允许java调用其他语言。
而compareAndSwapInt就是借助C来调用CPU底层指令实现的。
下面从分析比较常用的CPU（intel x86）来解释CAS的实现原理。
下面是sun.misc.Unsafe类的compareAndSwapInt()方法的源代码：

**这里讲的是大公司需要用到的一些高端Android技术，这里专门整理了一个文档，希望大家都可以看看。这些题目有点技术含量，需要好点时间去研究一下的。**

### 一、图片

##### **图片库对比**

| 图片库名称 | 实现原理 | 优点 | 缺点 |
| --- | --- | --- | --- |
| Fresco |  | 1.最大的优势便在于5.0以下(最低2.3) bitmap的加载，在5.0以下系统，Fresco将图片放到一个特别的内存区域(Ashmem)，而且图片不显示时，占用的内存会自动被释放，这会使APP更加流畅，减少因图片内存占用而引发的OOM。5.0以后的系统默认存储在Ashmem区了2.支持加载Git动态图和Webp格式的图片3.图片的渐进式呈现，图片先呈现大致的轮廓，然后随着图片下载的继续，逐渐成仙清晰的图片，这对于慢网络对说，用户体验更好。 | 框架体积比较大，3M左右，会增加APK的大小 |
| Glide |  | 1.图片默认格式为RGB565，而不是ARGB888，内存开销更小2.图片缓存的尺寸是和ImageView一样的，这使得图片加载更快3.支持加载Git动态图4.支持配置网络请求5.Glide的with()方法可以接收activity和fragment，图片的加载会和Activity和Fragment的生命周期保持一致 | Glide 显示动画会消耗很多内存 |
| Picasso |  | 1.图片质量高，但加载速度一般2.Picasso体积比起Glide小 | 只缓存一个全尺寸的图片 |

具体的可以参考这篇博客[Android的Glide库加载图片的用法及其与Picasso的对比](http://www.jb51.net/article/83152.htm)，虽然有点老了，新版本的api有许多变动，但是还是有参考价值的。（**以后有时间我把最新的Glide 4.+ 和 Picasso最新版 Fresco 做一个对比，写一篇博客介绍一下**）

##### 图片库的源码分析
##### 图片框架缓存实现
##### 图片加载原理
##### **自己去实现图片库，怎么做？**

	这里介绍一下思路，详细的可以看我给出的一个示例sample，自己手写的一个类似于Picasso的图片框架
	需要注意的几点：
	##### 1.首先要做好缓存，这是必备的：可以使用三级缓存，Android提供了LruCache：硬盘缓存用DiskLruCache，内存缓存用LRUCache。
	##### 2.图片加载需要压缩，可以考虑BitmapFactory做压缩
	##### 3.图片加载的来源，是从文件，还是网络
	##### 4.图片的异步加载和同步加载
	##### 知识点内存缓存 
	
### 二、网络和安全机制

##### [网络框架对比和源码分析](Android-source/谈谈对Okhttp的理解.md)
##### 自己去设计网络请求框架，怎么做？


##### 从网络加载一个10M的图片，说下注意事项

图片缓存、异常恢复、质量压缩
##### TCP与UDP基本区别

  1.基于连接与无连接
  2.TCP要求系统资源较多，UDP较少； 
  3.UDP程序结构较简单 
  4.流模式（TCP）与数据报模式(UDP); 
  5.TCP保证数据正确性，UDP可能丢包 
  6.TCP保证数据顺序，UDP不保证 
　　
UDP应用场景：
  1.面向数据报方式
  2.网络数据大多为短消息 
  3.拥有大量Client
  4.对数据安全性无特殊要求
  5.网络负担非常重，但对响应速度要求高

##### 如何验证证书的合法性?

##### https中哪里用了对称加密，哪里用了非对称加密，对加密算法（如RSA）等是否有了解?
https://showme.codes/2017-02-20/understand-https/
##### client如何确定自己发送的消息被server收到?
让对方收到之后回个话呗、HTTP协议里，有请求就有响应，根据响应的状态吗就能知道拉
##### WebSocket与socket的区别
Socket其实并不是一个协议，而是为了方便使用TCP或UDP而抽象出来的一层，是位于应用层和传输控制层之间的一组接口。

> Socket是应用层与TCP/IP协议族通信的中间软件抽象层，它是一组接口。在设计模式中，Socket其实就是一个门面模式，它把复杂的TCP/IP协议族隐藏在Socket接口后面，对用户来说，一组简单的接口就是全部，让Socket去组织数据，以符合指定的协议。

当两台主机通信时，必须通过Socket连接，Socket则利用TCP/IP协议建立TCP连接。TCP连接则更依靠于底层的IP协议，IP协议的连接则依赖于链路层等更低层次。

WebSocket则是一个典型的应用层协议。

![](http://7xrk8u.com1.z0.glb.clouddn.com/004552uSA.gif)

* 区别

**Socket是传输控制层协议，WebSocket是应用层协议。**##### 谈谈你对安卓签名的理解。


#### 请解释安卓为啥要加签名机制?
代码或者数据共享：Android提供了基于签名的权限机制
升级、应用程序模块化：


#### 视频加密传输

视频加密技术分为两种：
防盗链：通过验证的用户才能访问到没有加密的视频内容，这种方案存在视频很容易就被下载的风险，严格来说这不属于加密。这种方式其实是资源访问授权，它实现起来简单。
加密视频本身：通过对称加密算法加密视频内容本身，用户获得加密后的视频内容，通过验证的用户可以获取解密视频的密钥，在客户端解密后播放。这种方式实现起来流程复杂会带来更多的计算量。
一般结合这两种技术一起用，第1种技术很成熟也有很多教程就不再复述，本文主要介绍第2种加密技术。

**HLS 加密**

HLS 是目前最成熟的支持流媒体加密的能应用在浏览器里的流媒体传输协议，HLS 原生支持加密，下面来详细介绍它。

在介绍如何加密 HLS 先了解下 HLS 相比于其它流媒体传输协议的优缺点。
优点在于：

建立在 HTTP 之上，使用简单，接入代价小。
分片技术有利于 CDN 加速技术的实施。
部分浏览器原生支持，支持点播和录播。
缺点在于：

用作直播时延迟太大。
移动端支持还好，PC端只有 Safari 原生支持。



#### App 是如何沙箱化，为什么要这么做？

* Android沿用Linux权限模型
应用权限与文件权限机制、Android系统沿用了Linux的UID/GID权限模型，即用户ID和用户组ID，除了AID，Android还使用了辅助用户组机制，以允许进程访问共享或受保护的资源。
* Android沙箱模型
应用程序在独立的进程、应用程序在同一个进程（共享UID）

##### 权限管理系统（底层的权限是如何进行 grant 的）？


### 三、数据库

#### sqlite升级，增加字段的语句

```
  if(newVersion>oldVersion)
    {
        db.execSQL("ALTER TABLE local_picc_talk ADD  talknumber varchar(20);");
    }

```

##### 数据库框架对比和源码分析


#### 数据库的优化
* 建立索引、索引会增加db大小。
* 显式使用事务
* 查询数据优化
* 耗时异步化
* ContentValues的容量调整
>SQLiteDatabase提供了方便的ContentValues简化了我们处理列名与值的映射，ContentValues内部采用了HashMap来存储Key-Value数据，ContentValues的初始容量是8，如果当添加的数据超过8之前，则会进行双倍扩容操作，因此建议对ContentValues填入的内容进行估量，设置合理的初始化容量，减少不必要的内部扩容操作。

* 编译SQL语句
>SQLite想要执行操作，需要将程序中的sql语句编译成对应的SQLiteStatement，比如select ##### from record这一句，被执行100次就需要编译100次。对于批量处理插入或者更新的操作，我们可以使用显式编译来做到重用SQLiteStatement,参考以下代码。

```
private void insertWithPreCompiledStatement(SQLiteDatabase db) {
    String sql = "INSERT INTO " + TableDefine.TABLE_RECORD + "( " + TableDefine.COLUMN_INSERT_TIME + ") VALUES(?)";
    SQLiteStatement  statement = db.compileStatement(sql);
    int count = 0;
    while (count < 100) {
        count++;
        statement.clearBindings();
        statement.bindLong(1, System.currentTimeMillis());
        statement.executeInsert();
    }
}
```

##### 数据库数据迁移问题
 表升级，数据也要迁移

### 四、算法

##### 排序算法有哪些？
##### 最快的排序算法是哪个？
##### 手写一个冒泡排序
##### 手写快速排序代码
##### 快速排序的过程、时间复杂度、空间复杂度
##### 手写堆排序
##### 堆排序过程、时间复杂度及空间复杂度
##### 写出你所知道的排序算法及时空复杂度，稳定性
##### 二叉树给出根节点和目标节点，找出从根节点到目标节点的路径
##### 给阿里2万多名员工按年龄排序应该选择哪个算法？
##### GC算法(各种算法的优缺点以及应用场景)
##### 蚁群算法与蒙特卡洛算法
##### 子串包含问题(KMP 算法)写代码实现
##### 一个无序，不重复数组，输出N个元素，使得N个元素的和相加为M，给出时间复杂度、空间复杂度。手写算法
##### 万亿级别的两个URL文件A和B，如何求出A和B的差集C(提示：Bit映射->hash分组->多文件读写效率->磁盘寻址以及应用层面对寻址的优化)
##### 百度POI中如何试下查找最近的商家功能(提示：坐标镜像+R树)。
##### 两个不重复的数组集合中，求共同的元素。
##### 两个不重复的数组集合中，这两个集合都是海量数据，内存中放不下，怎么求共同的元素？
##### 一个文件中有100万个整数，由空格分开，在程序中判断用户输入的整数是否在此文件中。说出最优的方法
##### 一张Bitmap所占内存以及内存占用的计算
##### 2000万个整数，找出第五十大的数字？
##### 烧一根不均匀的绳，从头烧到尾总共需要1个小时。现在有若干条材质相同的绳子，问如何用烧绳的方法来计时一个小时十五分钟呢？
##### 求1000以内的水仙花数以及40亿以内的水仙花数
##### 5枚硬币，2正3反如何划分为两堆然后通过翻转让两堆中正面向上的硬8币和反面向上的硬币个数相同
##### 时针走一圈，时针分针重合几次
##### N*N的方格纸,里面有多少个正方形
##### x个苹果，一天只能吃一个、两个、或者三个，问多少天可以吃完？

### 五、插件化、模块化、组件化、热修复、增量更新、Gradle

##### 对热修复和插件化的理解
##### 插件化原理分析
##### 模块化实现（好处，原因）
##### 热修复,插件化
##### 项目组件化的理解
##### 描述清点击 Android Studio 的 build 按钮后发生了什么
* 检查项目和读取基本配置
* Gradle Build 
* Apk Install &  LaunchActivity  

> 将代码打包成APK，这里面涉及到编译、打包、签名、混淆等；安装APK到设备；在设备上运行APK

### 六、架构设计和设计模式

##### 谈谈你对Android设计模式的理解
##### MVC MVP MVVM原理和区别
##### 你所知道的设计模式有哪些？
##### 项目中常用的设计模式
##### 手写生产者/消费者模式
##### 写出观察者模式的代码
##### 适配器模式，装饰者模式，外观模式的异同？

* 装饰器模式：能动态的新增或组合对象的行为。 
* 代理模式：为其他对象提供一种代理以控制对这个对象的访问. 
* 适配器模式：是对其他对象接口的一种转换行为，将原接口转换为目标接口，达到适配的效果。
* 外观模式：外观对象提供对子系统各元件功能的简化为共同层次的调用接口，它主要起到"简化作用"。

> 装饰模式是“新增行为”，代理模式是“控制访问行为”，适配器模式是"转换行为"，外观模式是一种"简化行为"。

##### 用到的一些开源框架，介绍一个看过源码的，内部实现过程。
##### 谈谈对RxJava的理解
##### RxJava的功能与原理实现
##### RxJava的作用，与平时使用的异步操作来比的优缺点
##### 说说EventBus作用，实现方式，代替EventBus的方式
##### 从0设计一款App整体架构，如何去做？
##### 说一款你认为当前比较火的应用并设计(比如：直播APP，P2P金融，小视频等)
##### 谈谈对java状态机理解
##### Fragment如果在Adapter中使用应该如何解耦？
##### Binder机制及底层实现
##### 对于应用更新这块是如何做的？(解答：灰度，强制更新，分区域更新)？
##### 实现一个Json解析器(可以通过正则提高速度)
##### 统计启动时长,标准

### 七、性能优化

##### 如何对Android 应用进行性能分析以及优化?
https://www.jianshu.com/p/da2a4bfcba68 
##### ddms 和 traceView
##### 性能优化如何分析systrace？
https://www.jianshu.com/p/6f528e862d31 
##### 怎么保证应用启动不卡顿？


* [背景:Android App优化, 要怎么做?](https://www.jianshu.com/p/f7006ab64da7)
* [Android App优化之性能分析工具](https://www.jianshu.com/p/da2a4bfcba68)
* [Android App优化之提升你的App启动速度之理论基础](https://www.jianshu.com/p/98c1656a357a)
* [Android App优化之提升你的App启动速度之实例挑战](https://www.jianshu.com/p/4f10c9a10ac9)
* [Android App优化之Layout怎么摆](https://www.jianshu.com/p/4943dae4c333)
* [Android App优化之ANR详解](https://www.jianshu.com/p/6d855e984b99)
* [Android App优化之消除卡顿](https://www.jianshu.com/p/1fb065c806e6)
* [Android App优化之内存优化](https://www.jianshu.com/p/48475df838d9)
* [Android App优化之持久电量](https://www.jianshu.com/p/c55ef05c0047)
* [Android App优化之如何高效网络请求](https://www.jianshu.com/p/d4c2c62ffc35)

##### App启动崩溃异常捕捉
个应用异常捕获类AppUncaughtExceptionHandler，它必须得实现Thread.UncaughtExceptionHandler接口
##### 自定义View注意事项
减少不必要的调用invalidate()方法 
https://blog.csdn.net/whb20081815/article/details/74474736
 
* 降低刷新频率（任何时候执行requestLayout()，会使得Android UI系统去遍历整个View的层级来计算出每一个view的大小）
* 使用硬件加速

##### 现在下载速度很慢,试从网络协议的角度分析原因,并优化(提示：网络的5层都可以涉及)。
https://www.cnblogs.com/mylanguage/p/5635524.html
##### Https请求慢的解决办法（提示：DNS，携带数据，直接访问IP）
https://www.cnblogs.com/mylanguage/p/5635524.html
##### 如何保持应用的稳定性
内存，布局优化，代码质量，数据结构效率，针对业务合理的设计框架 
##### RecyclerView和ListView的性能对比
https://blog.csdn.net/fanenqian/article/details/61191532
##### RecycleView优化
https://blog.csdn.net/axi295309066/article/details/52741810
##### View渲染
https://www.cnblogs.com/ldq2016/p/6668148.html
##### java中的四种引用的区别以及使用场景

1. 强引用(StrongReference)
 强引用是使用最普遍的引用。如果一个对象具有强引用，那垃圾回收器绝不会回收它。当内存空间不足，Java虚拟机宁愿抛出OutOfMemoryError错误，使程序异常终止，也不会靠随意回收具有强引用的对象来解决内存不足的问题。

2. 软引用(SoftReference)
如果一个对象只具有软引用，则内存空间足够，垃圾回收器就不会回收它；如果内存空间不足了，就会回收这些对象的内存。只要垃圾回收器没有回收它，该对象就可以被程序使用。软引用可用来实现内存敏感的高速缓存。

软引用可以和一个引用队列（ReferenceQueue）联合使用，如果软引用所引用的对象被垃圾回收器回收，Java虚拟机就会把这个软引用加入到与之关联的引用队列中。

3. 弱引用(WeakReference)
弱引用与软引用的区别在于：只具有弱引用的对象拥有更短暂的生命周期。在垃圾回收器线程扫描它所管辖的内存区域的过程中，一旦发现了只具有弱引用的对象，不管当前内存空间足够与否，都会回收它的内存。不过，由于垃圾回收器是一个优先级很低的线程，因此不一定会很快发现那些只具有弱引用的对象。

弱引用可以和一个引用队列（ReferenceQueue）联合使用，如果弱引用所引用的对象被垃圾回收，Java虚拟机就会把这个弱引用加入到与之关联的引用队列中。  

4. 虚引用(PhantomReference)
"虚引用"顾名思义，就是形同虚设，与其他几种引用都不同，虚引用并不会决定对象的生命周期。如果一个对象仅持有虚引用，那么它就和没有任何引用一样，在任何时候都可能被垃圾回收器回收。

虚引用主要用来跟踪对象被垃圾回收器回收的活动。虚引用与软引用和弱引用的一个区别在于：虚引用必须和引用队列 （ReferenceQueue）联合使用。当垃圾回收器准备回收一个对象时，如果发现它还有虚引用，就会在回收对象的内存之前，把这个虚引用加入到与之 关联的引用队列中。  

总结：
WeakReference与SoftReference都可以用来保存对象的实例引用，这两个类与垃圾回收有关。
WeakReference是弱引用，其中保存的对象实例可以被GC回收掉。这个类通常用于在某处保存对象引用，而又不干扰该对象被GC回收，通常用于Debug、内存监视工具等程序中。因为这类程序一般要求即要观察到对象，又不能影响该对象正常的GC过程。
最近在JDK的Proxy类的实现代码中也发现了Weakrefrence的应用，Proxy会把动态生成的Class实例暂存于一个由Weakrefrence构成的Map中作为Cache。

SoftReference是强引用，它保存的对象实例，除非JVM即将OutOfMemory，否则不会被GC回收。这个特性使得它特别适合设计对象Cache。对于Cache，我们希望被缓存的对象最好始终常驻内存，但是如果JVM内存吃紧，为了不发生OutOfMemoryError导致系统崩溃，必要的时候也允许JVM回收Cache的内存，待后续合适的时机再把数据重新Load到Cache中。这样可以系统设计得更具弹性。

##### 强引用置为null，会不会被回收？

会，GC执行时，就被回收掉，前提是没有被引用的对象 
##### LinearLayout和RelativeLayout的性能对比
（1）RelativeLayout慢于LinearLayout是因为它会让子View调用2次measure过程，而LinearLayout只需一次，但是有weight属性存在时，LinearLayout也需要两次measure。

（2）RelativeLayout的子View如果高度和RelativeLayout不同，会导致RelativeLayout在onMeasure()方法中做横向测量时，纵向的测量结果尚未完成，只好暂时使用自己的高度传入子View系统。而父View给子View传入的值也没有变化就不会做无谓的测量的优化会失效，解决办法就是可以使用padding代替margin以优化此问题。

（3）在不响应层级深度的情况下，使用Linearlayout而不是RelativeLayout。



结论中的第三条也解释了文章前言中的问题：DecorView的层级深度已知且固定的，上面一个标题栏，下面一个内容栏，采用RelativeLayout并不会降低层级深度，因此这种情况下使用LinearLayout效率更高。

而为开发者默认新建RelativeLayout是希望开发者能采用尽量少的View层级，很多效果是需要多层LinearLayout的嵌套，这必然不如一层的RelativeLayout性能更好。因此我们应该尽量减少布局嵌套，减少层级结构，使用比如viewStub，include等技巧。

### 八、NDK、jni、Binder、AIDL、进程通信有关

##### 如何在jni中注册native函数，有几种注册方式?
静态、动态注册方法
##### Java如何调用c、c++语言？
##### jni如何调用java层代码？

C代码回调Java方法步骤：
①获取字节码对象（jclass (*FindClass)(JNIEnv*, const char*);）

②通过字节码对象找到方法对象（jmethodID (*GetMethodID)(JNIEnv*, jclass, const char*, const char*);）

③通过字节码文件创建一个object对象（该方法可选，方法中已经传递一个object，如果需要调用的方法与本地方法不在同一个文件夹则需要新创建object（jobject (*AllocObject)(JNIEnv*, jclass);），如果需要反射调用的java方法与本地方法不在同一个类中，需要创建该方法，但是如果是这样，并且需要跟新UI操作，例如打印一个Toast 会报空指针异常，因为这时候调用的方法只是一个方法，没有actiivty的生命周期。（下面有解决方案））

④通过对象调用方法，可以调用空参数方法，也可以调用有参数方法，并且将参数通过调用的方法传入（void (*CallVoidMethod)(JNIEnv*, jobject, jmethodID, ...);）

##### Binder机制
* IPC:
    ![IPC机制](http://7xrk8u.com1.z0.glb.clouddn.com/3985563-a3722ee387793114.png)
    不同进程之间彼此是不能共享的，而内核空间却是可共享的。Client进程向Server进程通信，利用进程间可共享的内核内存空间来完成底层通信工作的，Client端与Server端进程往往采用ioctl等方法跟内核空间的驱动进行交互。
* Binder原理:
    Binder通信采用C/S架构，包含Client、Server、ServiceManager以及binder驱动，其中ServiceManager用于管理系统中的各种服务。架构图如下所示：
    ![Binder同学架构图](http://7xrk8u.com1.z0.glb.clouddn.com/3985563-5ff2c4816543c433.jpg)

> #### [](http://zhaoxueli.win/2017/09/14/Android%E9%9D%A2%E8%AF%95%E6%80%BB%E7%BB%93/#Binder-%E5%9B%9B%E4%B8%AA%E8%A7%92%E8%89%B2%EF%BC%9A "Binder 四个角色：")Binder 四个角色：
> 
> 1. Client进程：使用服务的进程
> 2. server 进程：提供服务的进程
> 3. ServiceManager 进程：将字符型是的 Binder 名字转为 Client 中对该 Binder 的引用，使得 Client 能够通过 Binder 名字获取到 Server 中 Binder 实体的引用。
> 4. Binder 驱动：进程间 Binder 通信的建立，Binder 在进程间的传递，Binder 引用计数管理，数据包在进程间传递和交互等一系列底层支持。
>     
>     #### [](http://zhaoxueli.win/2017/09/14/Android%E9%9D%A2%E8%AF%95%E6%80%BB%E7%BB%93/#Binder-%E8%BF%90%E8%A1%8C%E6%9C%BA%E5%88%B6%EF%BC%9A "Binder 运行机制：")Binder 运行机制：
>     
>     
> 5. 注册服务：Server 在 ServiceManager 注册服务
> 6. 获取服务：Client 从 ServiceManager 获取相应的 Service
> 7. 使用服务：Client 根据得到的 Service 信息建立与 Service 所在的 Server 进程通信的通路可与 Server 进行交互。
##### 简述IPC？


* 系统实现。
* AIDL（Android Interface Definition Language，Android接口定义语言）：大部分应用程序不应该使用AIDL去创建一个绑定服务，因为它需要多线程能力，并可能导致一个更复杂的实现。
* Messenger：利用Handler实现。（适用于多进程、单线程，不需要考虑线程安全），其底层基于AIDL。

##### Android 上的 Inter-Process-Communication 跨进程通信时如何工作的？

##### Android进程分类？

系统强制销毁进程时，面临一个问题：**系统当中可能会运行多个进程，销毁哪些进程合适呢？** 这就是我们要讨论的进程的优先级问题。进程被系统强制销毁时，是按照进程的优先级进行的。而**进程的优先级主要和应用包含的组件相关**。进程优先级从高到低可分为四种：**前台进程**、**可视进程**、**服务进程**、**缓存进程**。

**前台进程（foreground process）**：需要用户当前正在进行的操作。一般满足以下条件：
1\. 屏幕顶层运行Activity（处于onResume()状态），用户正与之交互
2\. 有BroadcastReceiver正在执行代码
3\. 有Service在其回调方法（onCreate()、onStart()、onDestroy()）中正在执行代码
这种进程较少，一般来作为最后的手段来回收内存

**可视进程（visible process）**：做用户当前意识到的工作。一般满足以下条件：
1\. 屏幕上显示Activity，但不可操作（处于onPause()状态）
2\. 有service通过调用Service.startForeground()，作为一个前台服务运行
3\. 含有用户意识到的特定的服务，如动态壁纸、输入法等
这些进程很重要，一般不会杀死，除非这样做可以使得所有前台进程存活。

**服务进程（service process）**：含有以startService()方法启动的service。虽然该进程用户不直接可见，但是它们一般做一些用户关注的事情（如数据的上传与下载）。
这些进程一般不会杀死，除非系统内存不足以保持前台进程和可视进程的运行。
对于长时间运行的service（如30分钟以上），系统会考虑将之降级为缓存进程，避免长时间运行导致内存泄漏或其他问题，占用过多RAM以至于系统无法分配充足资源给缓存进程。

**缓存/后台进程（cached/background process）**:一般来说包含以下条件：
1\. 包含多个Activity实例，但是都不可见（处于onStop()且已返回）。
系统如有内存需要，可随意杀死。

##### 进程和 Application 的生命周期？
##### 进程调度
https://blog.csdn.net/innost/article/details/6940136
##### 谈谈对进程共享和线程安全的认识
https://blog.csdn.net/coding_glacier/article/details/8230159 
https://blog.csdn.net/oweixiao123/article/details/9057445 
问你线程安全的时候，不止要回答主线程跟子线程之间的切换，还有数据结构处理的线程安全问题，多线程操作同一个数据的一致性问题，等等。

##### 谈谈对多进程开发的理解以及多进程应用场景
##### 什么是协程？
协程是一种用户态的轻量级线程，协程的调度完全由用户控制。协程拥有自己的寄存器上下文和栈。协程调度切换时，将寄存器上下文和栈保存到其他地方，在切回来的时候，恢复先前保存的寄存器上下文和栈，直接操作栈则基本没有内核切换的开销，可以不加锁的访问全局变量，所以上下文的切换非常快。

### 九、framework层、ROM定制、Ubuntu、Linux之类的问题

##### java虚拟机的特性
##### 谈谈对jvm的理解
##### JVM内存区域，开线程影响哪块内存
##### 对Dalvik、ART虚拟机有什么了解？
##### Art和Dalvik对比
##### 虚拟机原理，如何自己设计一个虚拟机(内存管理，类加载，双亲委派)
##### 谈谈你对双亲委派模型理解
##### JVM内存模型，内存区域
##### 类加载机制
##### 谈谈对ClassLoader(类加载器)的理解
##### 谈谈对动态加载（OSGI）的理解
OSGI作为一个成熟的面向模块化的框架标准被许多诸
##### 内存对象的循环引用及避免
java采用可达性分析回收垃圾对象，循环引用并不会使对象无法被回收。
##### 内存回收机制、GC回收策略、GC原理时机以及GC对象
##### 垃圾回收机制与调用System.gc()区别
https://blog.csdn.net/jiyiqini/article/details/46725647
##### Ubuntu编译安卓系统
##### 系统启动流程是什么？（提示：Zygote进程 –> SystemServer进程 –> 各种系统服务 –> 应用进程）
##### 大体说清一个应用程序安装到手机上时发生了什么
##### 简述Activity启动全部过程
##### App启动流程，从点击桌面开始
##### 逻辑地址与物理地址，为什么使用逻辑地址？
##### Android为每个应用程序分配的内存大小是多少？
##### Android中进程内存的分配，能不能自己分配定额内存？
##### 进程保活的方式
##### 如何保证一个后台服务不被杀死？（相同问题：如何保证service在后台不被kill？）比较省电的方式是什么？
https://segmentfault.com/a/1190000011905950
##### App中唤醒其他进程的实现方式

