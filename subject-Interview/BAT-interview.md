##Alibaba
---

一面

* 说一下你怎么学习安卓的？
* 项目中遇到哪些问题，如何解决的？
* Android事件分发机制？
* 三级缓存底层实现？
* HashMap底层实现，hashCode如何对应bucket?
* Java的垃圾回收机制，引用计数法两个对象互相引用如何解决？
* 用过的开源框架的源码分析
* Acticity的生命周期，Activity异常退出该如何处理？
* tcp和udp的区别，tcp如何保证可靠的，丢包如何处理？

二面：

* 标号1-n的n个人首尾相接，1到3报数，报到3的退出，求最后一个人的标号
* 给定一个字符串，求第一个不重复的字符 abbcad -> c


面试者：陶程

## 阿里面试题

#### LeakCanary 实现原理
[LeakCanary核心原理源码浅析](http://blog.csdn.net/cloud_huan/article/details/53081120)

#### 内存泄露的本质
无法回收无用的对象

#### Handler 队列阻塞算法、在Android中的地位、如何自己实现？
[Android应用程序消息处理机制（Looper、Handler）分析](http://blog.csdn.net/luoshengyang/article/details/6817933)

#### C 和 java 如何通信？JNI原理
[Java JNI实现原理初探](http://blog.csdn.net/hackooo/article/details/48395765/)

#### 比较熟悉的开源项目源码分析
okhttp、glide

#### 线程池定制、源码分析
- [java线程池的使用](http://yukai.space/2017/05/08/java%E7%BA%BF%E7%A8%8B%E6%B1%A0%E7%9A%84%E4%BD%BF%E7%94%A8/)
- [Java 线程池的理论与实践](https://juejin.im/post/5906b6e78d6d810058dab1bf)

#### 虚拟机如何实现的synchronized？
[Java SE1.6中的Synchronized](http://www.infoq.com/cn/articles/java-se-16-synchronized)

#### 一个文件中有100万个整数，由空格分开，在程序中判断用户输入的整数是否在此文件中。说出最优的方法

#### 两个进程同时要求写或者读，能不能实现？如何防止进程的同步？

#### volatile 的意义？

防止CPU指令重排序

#### 单例

```java
public class Singleton{
private volatile static Singleton mSingleton;
private Singleton(){
}
public static Singleton getInstance(){
  if(mSingleton == null){\\A
    synchronized(Singleton.class){\\C
     if(mSingleton == null)
      mSingleton = new Singleton();\\B
      }
    }
    return mSingleton;
  }
}
```

#### Given a string, determine if it is a palindrome（回文，如果不清楚，按字面意思脑补下）, considering only alphanumeric characters and ignoring cases.  

For example,  "A man, a plan, a canal: Panama" is a palindrome.  "race a car" is not a palindrome.  

Note:  Have you consider that the string might be empty? This is a good question to ask during an interview.  For the purpose of this problem, we define empty string as valid palindrome.

```java
public boolean isPalindrome(String palindrome){
		char[] palindromes = palidrome.toCharArray();
 		if(palindromes.lengh == 0){
	 		return true
 		}
 		Arraylist<Char> temp = new Arraylist();
 		for(int i=0;i<palindromes.length;i++){
 		if((palindromes[i]>'a' && palindromes[i]<'z')||palindromes[i]>'A' && palindromes[i]<'Z')){
 		temp.add(palindromes[i].toLowerCase());
 		}
		}
 		for(int i=0;i<temp.size()/2;i++){
 		if(temp.get(i) != temp.get(temp.size()-i)){
 		//
 		return false;
 		}
 		}
 		return true;
}
```

#### 烧一根不均匀的绳，从头烧到尾总共需要1个小时。现在有若干条材质相同的绳子，问如何用烧绳的方法来计时一个小时十五分钟呢
用两根绳子，一个绳子两头烧，一个一头烧。


## 网络
目前大多数应用中都会使用一些开源网络库，对于此我们不仅要知道是什么，而且也要知道为什么。

**流行网络库对比**
- [comparison-of-android-networking-libraries-okhttp-retrofit-and-volley](http://stackoverflow.com/questions/16902716/comparison-of-android-networking-libraries-okhttp-retrofit-and-volley)
- [Android Async HTTP Clients: Volley vs Retrofit](http://blog.csdn.net/hwz2311245/article/details/46845271)
- [Android实战之你应该使用哪个网络库？](https://segmentfault.com/a/1190000003965158)

**源码解析**

- [Volley](http://p.codekk.com/blogs/detail/54cfab086c4761e5001b2542)
- [okhttp](http://www.jianshu.com/p/aad5aacd79bf)
- [retrofit](https://github.com/android-cn/android-open-project-analysis/tree/master/tool-lib/network/retrofit)

## 腾讯
#### 2000万个整数，找出第五十大的数字？
冒泡、选择、建堆
#### 从网络加载一个10M的图片，说下注意事项
图片缓存、异常恢复、质量压缩
#### 自定义View注意事项
渲染帧率、内存
#### 项目中常用的设计模式
单例、观察者、适配器、建造者。。
#### JVM的理解
[http://www.infoq.com/cn/articles/java-memory-model-1
](http://www.infoq.com/cn/articles/java-memory-model-1)

## 百度
#### 笔试题
1. 冒泡排序
2. 四大组件的生命周期和简单用法
3. 图片框架缓存实现
3. 设计网络请求框架

#### 问答
1. LruCache默认缓存大小
2. httpurlconnection 和 okhttp关系（HTTPurlconnection 底层基于OKHTTP）
3. 开源库源码分析
4. 从0设计一款App整体架构（可以结合微信和淘宝架构演进方面进行回答）


## 美团
---

一面

- 自我介绍
- 面向对象三大特性
- Java虚拟机，垃圾回收
- GSON
- RxJava+Retrofit
- 图片缓存，三级缓存
- Android启动模式
- 四大组件
- Fragment生命周期，嵌套
- AsyncTask机制
- Handler机制

二面

- 面试官写程序，看错误。
- 面试官写程序让判断GC引用计数法循环引用会发生什么情况
- Android进程间通信，Binder机制
- Handler消息机制，postDelayed会造成线程阻塞吗？对内存有什么影响？
- Debug和Release状态的不同
- 实现stack 的pop和push接口 要求：
	- 1.用基本的数组实现
	- 2.考虑范型
	- 3.考虑下同步问题
	- 4.考虑扩容问题

面试者:陶程

## 新浪微博
---
一面
---

静态内部类、内部类、匿名内部类，为什么内部类会持有外部类的引用？持有的引用是this？还是其它？

```
静态内部类：使用static修饰的内部类
匿名内部类：使用new生成的内部类
因为内部类的产生依赖于外部类，持有的引用是类名.this。
```

ArrayList和Vector的主要区别是什么？

```
ArrayList在Java1.2引入，用于替换Vector

Vector:

线程同步
当Vector中的元素超过它的初始大小时，Vector会将它的容量翻倍

ArrayList:

线程不同步，但性能很好
当ArrayList中的元素超过它的初始大小时，ArrayList只增加50%的大小
```

[Java集合类框架](http://yuweiguocn.github.io/2016/01/06/java-collection/)


Java中try catch finally的执行顺序

```
先执行try中代码发生异常执行catch中代码，最后一定会执行finally中代码
```

switch是否能作用在byte上，是否能作用在long上，是否能作用在String上？

```
switch支持使用byte类型，不支持long类型，String支持在java1.7引入
```

Activity和Fragment生命周期有哪些？

```
Activity——onCreate->onStart->onResume->onPause->onStop->onDestroy

Fragment——onAttach->onCreate->onCreateView->onActivityCreated->onStart->onResume->onPause->onStop->onDestroyView->onDestroy->onDetach
```


onInterceptTouchEvent()和onTouchEvent()的区别？

```
onInterceptTouchEvent()用于拦截触摸事件
onTouchEvent()用于处理触摸事件
```

RemoteView在哪些功能中使用

```
APPwidget和Notification中
```

SurfaceView和View的区别是什么？

```
SurfaceView中采用了双缓存技术，在单独的线程中更新界面
View在UI线程中更新界面
```

讲一下android中进程的优先级？

```
前台进程
可见进程
服务进程
后台进程
空进程
```

tips：静态类持有Activity引用会导致内存泄露


##二面

* service生命周期，可以执行耗时操作吗？
* JNI开发流程
* Java线程池，线程同步
* 自己设计一个图片加载框架
* 自定义View相关方法
* http ResponseCode
* 插件化，动态加载
* 性能优化，MAT
* AsyncTask原理
* 65k限制
* Serializable和Parcelable
* 文件和数据库哪个效率高
* 断点续传
* WebView和JS
* 所使用的开源框架的实现原理，源码

[codekk：开源框架源码解析](http://codekk.com/open-source-project-analysis)


[Android基础——Service](http://yuweiguocn.github.io/2016/03/28/android-basic-service/)

[Android基础——IntentService](http://yuweiguocn.github.io/2016/03/31/android-basic-intentservice/)

[Android开发指导——Service](http://yuweiguocn.github.io/2016/04/02/android-guide-service/)

[Android开发指导——绑定Service](http://yuweiguocn.github.io/2016/03/31/android-guide-bound-service/)

[Android开发指导——进程间通信AIDL](http://yuweiguocn.github.io/2016/03/31/android-guide-aidl/)

[Android面试基础知识总结（一）](http://yuweiguocn.github.io/2016/03/26/android-interview-basic-1/)

[Android面试——APP性能优化](http://yuweiguocn.github.io/2016/04/10/android-interview-peformance/)

[Android中Java和JavaScript交互](http://droidyue.com/blog/2014/09/20/interaction-between-java-and-javascript-in-android/)

[WebView 远程代码执行漏洞浅析](http://jaq.alibaba.com/blog.htm?spm=0.0.0.0.oMsDAl&id=48)

[WebView中的Java与JavaScript提供【安全可靠】的多样互通方案](https://github.com/pedant/safe-java-js-webview-bridge)


